#include "../sharedlib/protocol.hpp"

#include <catch2/catch_test_macros.hpp>

#include <stdint.h>
#include <vector>


TEST_CASE( "String Werte können wieder entnommen werden", "[writeRead]"){
    std::string text("This is a placeholder");
    std::string enc = dcs_protocol::setStringAttribute("Text", text);
    
    REQUIRE(dcs_protocol::getStringAttribute(enc) == text);
}

TEST_CASE( "Nummer Werte können wieder entnommen werden", "[writeRead]"){
    int val = 300;
    std::string enc = dcs_protocol::setNumericAttribute("Num", val);
    
    REQUIRE(dcs_protocol::getNumericAttribute(enc) == val);
}

TEST_CASE( "Boolean Werte können wieder entnommen werden", "[writeRead]"){
    bool first = true;
    std::string enc = dcs_protocol::setBooleanAttribute("Num", first);
    
    REQUIRE(dcs_protocol::getBooleanAttribute(enc) == first);
}

TEST_CASE( "Attributnamen können gelesen werden", "[writeRead]"){
    std::string text("This is a placeholder");
    std::string encStr = dcs_protocol::setStringAttribute("Text", text);
    
    int val = 300;
    std::string encNum = dcs_protocol::setNumericAttribute("Num", val);
    
    
    REQUIRE(dcs_protocol::getAttributeName(encStr) == "Text");
    REQUIRE(dcs_protocol::getAttributeName(encNum) == "Num");
}

TEST_CASE( "Farbwerte werden korrekt gelesen?", "[color]" ){
    uint8_t r = 255;
    uint8_t g = 110;
    uint8_t b = 12;
    
    std::vector<uint8_t> channels({r, g, b});
    
    std::string encoded = dcs_protocol::writeColor(channels);
    
    std::vector decoded = dcs_protocol::readColor(encoded);
    
    REQUIRE (decoded.size() == channels.size());
    REQUIRE (decoded[0] == channels[0]);
    REQUIRE (decoded[1] == channels[1]);
    REQUIRE (decoded[2] == channels[2]);
}

TEST_CASE( "Koordinaten werden korrekt gelesen?", "[coordinates]" ){
    int x = -20;
    int y = 100;
    
    std::vector<int> axis({x, y});
    
    std::string encoded = dcs_protocol::writeCoord(axis);
    
    std::vector decoded = dcs_protocol::readCoord(encoded);
    
    REQUIRE (decoded.size() == axis.size());
    REQUIRE (decoded[0] == axis[0]);
    REQUIRE (decoded[1] == axis[1]);
}

TEST_CASE( "Header kann gelesen werden", "[header]" ){
    std::string content = "placeholder";
    
    std::string function = "printText";
    std::string contentType = "Text";
    
    std::string message = dcs_protocol::addHeader(function, contentType, content);
    
    
    REQUIRE(message.size() > 0);
    
    REQUIRE(dcs_protocol::getFunctionFromHeader(message) == function);
    REQUIRE(dcs_protocol::getContentTypeFromHeader(message) == contentType);
    REQUIRE(dcs_protocol::getContentLengthFromHeader(message) == content.size());
    REQUIRE(dcs_protocol::getContent(message) == content);
}
