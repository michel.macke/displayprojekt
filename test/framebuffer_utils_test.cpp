#include "../Server/framebuffer_utils.h"

#include <catch2/catch_test_macros.hpp>

TEST_CASE( "Breite der Ausgabe korrekt?", "[byte_to_bit]" ){
    unsigned int len_x = 204;
    unsigned int len_y = 153;
    unsigned int predicted_len_x = (int) (len_x/8);
    if(len_x % 8 != 0){
    	++predicted_len_x;
    }
    
    std::vector<std::vector<uint8_t>> test_input(len_y, std::vector<uint8_t>(len_x, 0));
    
    std::vector< std::vector< uint8_t>> test_output = byte_to_bit(test_input, 0);
    
    REQUIRE (test_output.size() == len_y);
    REQUIRE (test_output[0].size() == predicted_len_x);
}

TEST_CASE( "Bytes korrekt zusammengefasst?" ,"[byte_to_bit]"){
    //Jedes übergebene Byte, dass über der Wertgrenze liegt, 
    //muss im Output für ein positives Bit sorgen
    //Wenn weniger als 8 Byte übergeben werden,
    //müssen die übrigen Bits 0 sein.
    std::vector< std::vector<uint8_t>> test_input ={  {255,    255,    200,    0,      255},
                                        {0,      255,    127,    245,    225}};
    
    std::vector<std::vector<uint8_t>> test_comparison = {{0b11101000},
                                                         {0b01011000}};

    REQUIRE (test_comparison == byte_to_bit(test_input, 128));
}

TEST_CASE( "Ausgabe hat gewünschte Dimensionen?", "[get_area]"){

    unsigned int size_x = 3;
    unsigned int size_y = 3;
    
    std::vector< std::vector< uint8_t > > test_input = {{50, 200, 180, 80},
                                                        {255, 90, 100, 110},
                                                        {12, 210, 210, 130},
                                                        {24, 165, 188, 231}};
    
    std::vector< std::vector< uint8_t > > test_output = get_area(test_input, size_x, size_y);
    
    REQUIRE (test_output[0].size() == size_x);
    REQUIRE (test_output.size() == size_y);
}

TEST_CASE("Es kann ein Bereich kleiner als der Inpu entnommen werden?", "[get_area]"){
    std::vector< std::vector< uint8_t > > test_input = {{50, 200, 180, 80},
                                                        {255, 90, 100, 110},
                                                        {12, 210, 210, 130},
                                                        {24, 165, 188, 231}};
    
    std::vector< std::vector< uint8_t > > test_comparison = {{50, 200, 180},
                                                            {255, 90, 100},
                                                            {12, 210, 210}};
    
    std::vector< std::vector< uint8_t > > test_output = get_area(test_input, 3, 3);
    
    REQUIRE( test_comparison == test_output );
}

TEST_CASE( "Es kann ein Bereich größer als der Input gefordert werden?", "[get_area]"){
    std::vector< std::vector< uint8_t > > test_input = {{50, 200, 180, 80},
                                                        {255, 90, 100, 110},
                                                        {12, 210, 210, 130},
                                                        {24, 165, 188, 231}};
    
    std::vector< std::vector< uint8_t > > test_comparison = {{50, 200, 180, 80, 0},
                                                            {255, 90, 100, 110, 0},
                                                            {12, 210, 210, 130, 0},
                                                            {24, 165, 188, 231, 0},
                                                            {0,  0,   0,   0,   0}};
    
    std::vector< std::vector< uint8_t > > test_output = get_area(test_input, 5, 5);

    REQUIRE (test_comparison == test_output);
}

TEST_CASE( "Es kann ein Bereich ab positiven Startkoordinaten gefordert werden?", "[get_area]"){
    std::vector< std::vector< uint8_t > > test_input = {{50, 200, 180, 80},
                                                        {255, 90, 100, 110},
                                                        {12, 210, 210, 130},
                                                        {24, 165, 188, 231}};
    
    std::vector< std::vector< uint8_t > > test_comparison = {{90, 100, 110},
                                                            {210, 210, 130},
                                                            {165, 188, 231}};
    
    std::vector< std::vector< uint8_t > > test_output = get_area(test_input, 3, 3, 1, 1);

    REQUIRE (test_comparison == test_output);
}

TEST_CASE( "Es kann ein Bereich ab negativen Statkoordinaten gewüscht werden?", "[get_area]"){
    std::vector< std::vector< uint8_t > > test_input = {{50, 200, 180, 80},
                                                        {255, 90, 100, 110},
                                                        {12, 210, 210, 130},
                                                        {24, 165, 188, 231}};
    
    std::vector< std::vector< uint8_t > > test_comparison = {{0, 0, 0},
                                                            {0, 50, 200},
                                                            {0, 255, 90}};
    
    std::vector< std::vector< uint8_t > > test_output = get_area(test_input, 3, 3, -1, -1);

    REQUIRE (test_comparison == test_output);
}
