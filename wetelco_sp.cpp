/*
Copyright (C) 2022  Michel Macke, Finn Wundram, Hassene Jeddi, Mohamed Salah Messai

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "wetelco_sp.h"

std::vector<uint8_t> embed_into_protocol_frame(std::vector<uint8_t> command, uint8_t target_display_id){
    std::vector<uint8_t> final_packet;

	final_packet.push_back(0x00);
    final_packet.push_back(0x00);   //Frame start
    final_packet.push_back(0x00);

    final_packet.push_back(target_display_id);

    final_packet.insert(final_packet.end(), command.begin(), command.end());    //Insert Content

    final_packet.push_back(0xFE);   //Frame end

    return final_packet;
}


std::vector<uint8_t> send_text(std::string page_number, uint8_t fade_in_effect, uint8_t fade_in_speed, uint8_t duration_of_visibility, uint8_t text_alignment, uint8_t text_color, uint8_t text_font, std::string text_content){
    std::vector<uint8_t> command;

    command.push_back(0x01);   //Simple Text
    command.push_back(0x02);   //STX
    command.push_back((uint8_t) "W");    //Write Data
    command.insert(command.end(), page_number.begin(), page_number.end());
    command.push_back(fade_in_effect);
    command.push_back(fade_in_speed);
    command.push_back(duration_of_visibility);

    command.push_back(text_alignment);
    command.push_back(0xFD);   //Change of format
    command.push_back(text_color);
    command.push_back(text_font);
    command.push_back(0x00);   //Reserved
    command.insert(command.end(), text_content.begin(), text_content.end());

    return command;
}


std::vector<uint8_t> send_image(std::string page_number, uint8_t fade_in_effect, uint8_t fade_in_speed, uint8_t duration_of_visibility, std::array<std::array<uint8_t, 32>, 32> bitmap_red, std::array< std::array< uint8_t, 32>, 32> bitmap_green, std::array<std::array<uint8_t, 32>, 32> bitmap_blue){
    std::vector<uint8_t> command;

    command.push_back(0x01);   //Simple Text
    command.push_back(0x02);   //STX
    command.push_back((uint8_t) "W");    //Write Data
    command.insert(command.end(), page_number.begin(), page_number.end());
    command.push_back(fade_in_effect);
    command.push_back(fade_in_speed);
    command.push_back(duration_of_visibility);

    std::array<std::array<uint8_t, 32>, 32>::iterator display_height_iterator;
    std::array<uint8_t, 32> currentRow;

    for(display_height_iterator = bitmap_red.begin(); display_height_iterator != bitmap_red.end();display_height_iterator++){
        currentRow = *display_height_iterator;
        command.insert(command.end(), currentRow.begin(), currentRow.end());
    }

    for(display_height_iterator = bitmap_green.begin(); display_height_iterator != bitmap_green.end();display_height_iterator++){
        currentRow = *display_height_iterator;
        command.insert(command.end(), currentRow.begin(), currentRow.end());
    }

    for(display_height_iterator = bitmap_blue.begin(); display_height_iterator != bitmap_blue.end();display_height_iterator++){
        currentRow = *display_height_iterator;
        command.insert(command.end(), currentRow.begin(), currentRow.end());
    }

    return command;
}


std::vector<uint8_t> send_playlist(uint8_t repetitions, std::string page_list){
    std::vector<uint8_t> command;

    command.push_back(0x1A);
    command.push_back(repetitions);
    command.push_back(0x02);
    command.insert(command.end(), page_list.begin(), page_list.end());

    return command;
}

std::vector<uint8_t> delete_playlist(){
    std::vector<uint8_t> command;

    command.push_back(0x1A);
    command.push_back(0x00);
    command.push_back(0x02);

    return command;

}

std::vector<uint8_t> delete_page(std::string page_list){
    std::vector<uint8_t> command;

    command.push_back(0x1B);
    command.push_back(0x02);
    command.insert(command.end(), page_list.begin(), page_list.end());

    return command;
}

std::vector<uint8_t> show_page(std::string page_number){
    std::vector<uint8_t> command;

    command.push_back(0x1E);
    command.push_back(0x02);
    command.insert(command.end(), page_number.begin(), page_number.end());

    return command;
}
