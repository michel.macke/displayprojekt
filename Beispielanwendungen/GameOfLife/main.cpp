/*
Copyright (C) 2022  Michel Macke, Finn Wundram, Hassene Jeddi, Mohamed Salah Messai

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include <stdlib.h>
#include <stdio.h>

#include <stdint.h>
#include <string> 


#include "toml.hpp"

#include <netinet/in.h>

#include <vector>
#include <string>

#include "Client.hpp"
#include "LoggingFacility.hpp"
#include "SimpleFileLogger.hpp"

bool isNumeric(std::string str) {
   for (int i = 0; i < str.length(); i++)
      if (isdigit(str[i]) == false)
         return false; //when one non numeric value is found, return false
   return true;
}
bool cellState(int posX, int posY, std::vector<std::vector<bool>> field)
{
    std::vector<std::vector<bool>> square = std::vector<std::vector<bool>>(3,std::vector<bool>(3, true));
    int state = 0;
    if(posX + 1 >= field[0].size())
    {
        square[0][2] = false;
        square[1][2] = false;
        square[2][2] = false;
    }
    if(posX - 1 < 0)
    {
        square[0][0] = false;
        square[1][0] = false;
        square[2][0] = false;
    }
    if(posY + 1 >= field.size())
    {
        square[2][0] = false;
        square[2][1] = false;
        square[2][2] = false;
    }
    if(posY - 1 < 0)
    {
        square[0][0] = false;
        square[0][1] = false;
        square[0][2] = false;
    }
    //square[posX][posY] = field[posX][posY];

    for(int y = 0; y < 3; y++)
    {
        for(int x = 0; x < 3; x++)
        {
            if(square[y][x])
            {
                if(field[posY+(y-1)][posX+(x -1)] == true)
                {
                    state++;
                }
            }
        }
    }
    if(state <= 4 && state > 2)
    {
        return true;
    }
    else
    {
        return false;
    }
    //check rules
    //Births: Each dead cell adjacent to exactly three live neighbors will become live in the next generation.
    //Death by isolation: Each live cell with one or fewer live neighbors will die in the next generation.
    //Death by overcrowding: Each live cell with four or more live neighbors will die in the next generation.
    //Survival: Each live cell with either two or three live neighbors will remain alive for the next generation.
}
int main(int argc, char* argv[]){
    Logger log = std::make_shared<SimpleFileLogger>("./gol_log.txt");
    Client c = Client("../res/server.toml", log);
    std::string text = "test";
    std::vector<char> sendtext;
    std::vector<int> screen = std::vector<int>(2);
    screen[0] = 16;
    screen[1] = 16;
    //std::vector<std::vector<bool>> game = std::vector<std::vector<bool>>(screen[1],std::vector<bool>(screen[0], false));
    std::vector<std::vector<bool>> game = std::vector<std::vector<bool>>(16,std::vector<bool>(16, false));
    std::vector<std::vector<bool>> nextRound = std::vector<std::vector<bool>>(16,std::vector<bool>(16, false));
    std::cout << "Welcome to the game of life\nThis game is quite simple\nChoose, where you want to place an organism\n";

    std::string input;
    c.setPixel(5,5,255, 255, 255);
    c.updateScreen(false);
    
    bool setup = true;

    while(setup)
    {
        bool inputCorret = false;
        int x = 0;
        
        
        while(!inputCorret)
        {
            std::cout << "You can place Life anywhere between X: ";
            std::cout << screen[0];
            std::cout << " and Y: "; 
            std::cout << screen[0] << std::endl;
            std::cout << "Please input the X value\n";
            getline(std::cin, input);
            try
            {
                if(isNumeric(input))
                {
                    x = stoi(input);
                    if(x > screen[0] || x <0)
                    {
                        std::cout << "Please input an X value bigger than 0 and smaller than";
                        std::cout << screen[0] << std::endl;
                    }
                    else
                    {
                        inputCorret = true;
                    }
                }
                else
                {
                    std::cout << "Please input a number";
                }
            }
            catch(const std::exception& e)
            {
                std::cerr << e.what() << std::endl;
            }
        }

        std::cout << "Please input the Y value\n";
        
        inputCorret = false;
        int y = 0;
        while(!inputCorret)
        {
            getline(std::cin, input);
            try
            {
                if(isNumeric(input))
                {
                    y = stoi(input);
                    std::cout << y << std::endl;
                    if(x > screen[0] || x <0)
                    {
                        std::cout << "Please input an Y value bigger than 0 and smaller than";
                        std::cout << screen[0] << std::endl;
                    }
                    else
                    {
                        inputCorret = true;
                    }
                }
                else
                {
                    std::cout << "Please input a number";
                }
            }
            catch(const std::exception& e)
            {
                std::cerr << e.what() << std::endl;
            }
        }
        std::cin.clear();
        game[y][x] = true;


        inputCorret = false;
        while(!inputCorret)
        {
            char conf;
            std::cout << "Do you want to add another life?(y/n)\n";
            std::cin.get(conf);
            
            
            if(conf == 'y' || conf == '\n')
            {
                inputCorret = true;
            }
            if(conf == 'n')
            {
                std::cout << "May the game begin\n";
                setup = false;
            }
            else
            {
                std::cout << "Please type y to confirm or n to stop adding life\n";
            }
        }
    }
    c.clearScreen();
    c.updateScreen(false);

    for(int y= 0; y < game.size();y++)
    {
        for(int x = 0; x < game[y].size(); x++)
        {
            if(game[y][x])
            {
                std::cout << "X";
                c.setPixel(x,y,0, 255, 0);
            }
            else
            {
            } 
        }
        std::cout << "\n";
    }
    bool inputCorret = false;
    while(!inputCorret)
    {
        std::cout << "Press any key to advance\n";
        char conf;
        std::cin.get(conf);
        inputCorret = true;
    }
    c.updateScreen(false);
    bool thereIsLife = true;
    while(thereIsLife)
    {
        c.clearScreen();
        thereIsLife = false;
        std::cout << "InWhile";
        for(int y= 0; y < game.size();y++)
        {
            for(int x = 0; x < game[y].size(); x++)
            {
                nextRound[y][x] = cellState(x,y,game);
            }
            
        }
        for(int y= 0; y < game.size();y++)
        {
            for(int x = 0; x < game[y].size(); x++)
            {
                game[y][x] = nextRound[y][x];
                if(game[y][x])
                {
                    thereIsLife = true;
                    std::cout << "X";
                    c.setPixel(x,y,255, 255, 255);
                    
                }
                else
                {
                }
            }
            std::cout << "\n";
        }
        c.updateScreen(false);
        bool inputCorret = false;
        while(!inputCorret)
        {
            std::cout << "Press any key to advance\n";
            char conf;
            std::cin.get(conf);
            inputCorret = true;
        }
    } 
}
