#include "Client.hpp"

#include <stdio.h>
#include <iostream>
#include <string>
#include <vector>
#include <stdint.h>

#include "PreProcessingStrings.hpp"
#include "LoggingFacility.hpp"
#include "SimpleFileLogger.hpp"

std::vector<int> coordinateInput();
std::vector<uint8_t> colorInput();
std::string getInput(std::string prompt);

int main(int argc, char* argv[]){    
    std::string ip_address;
    int port;
    
    auto serverConfig = toml::parse_file("../res/client.toml");
    ip_address = serverConfig["Port"].value_or("127.0.0.1");
    port = serverConfig["Port"].value_or(25565);
    
    if(argc > 1){
        for(int i = 1; i + 1 < argc; i++){
            if(std::strcmp(argv[i], "-IP") == 0){
                ip_address = argv[++i];
            } else if (std::strcmp(argv[i], "-Port") == 0){
                port = std::stoi(argv[++i]);
            }
        }
    }
    Logger log = std::make_shared<SimpleFileLogger>("./client_log.txt");
    Client c(ip_address, port, log);
    
    std::string input;
    std::vector< std::vector<int> > coordinates;
    std::vector<uint8_t> color;
    bool scrolling = false;
    
    std::cout << "This program requires a running server. Type \"help\", \"h\" or \"?\" to view commands.\n";
    
    while(true){
    std::cout << "Enter command:\n";
    getline(std::cin, input);
    //help h ?
        if((input == "help") ||
            (input == "help") ||
            (input == "help")){
            std::cout << "help, h, ?\tList available commands.\n";
            std::cout << "exit, quit\tEnd this program.\n";
            std::cout << "\n";
            std::cout << "stop\tStops both the server and this client.\n";
            std::cout << "size\tShows width and height of the framebuffer.\n";
            std::cout << "clear\tClear the framebuffer.\n";
            std::cout << "scroll\tToggle the display image between static and scrolling\n";
            std::cout << "set\tSets a single pixel to the desired color.\n";
            std::cout << "get\tShows the color of a single pixel\n";
            std::cout << "line\tDraws a line of the desired color between two points\n";
            std::cout << "poly\tDraw closed chain of lines in desired color.\n";
            std::cout << "circle\tDraw circle in desired color.\n";
            std::cout << "triangle\tDraw triangle between points.\n";
            std::cout << "quad\tDraw quad between points.\n";
            std::cout << "write\tWrite Text to the screen.\n";
            std::cout << "image\tLoad image file onto the screen.\n";
            std::cout << "\n";
        }
    //exit quit
        else if((input == "exit")||
            (input == "quit")){
            break;
        }
        
    //stopServer
    
        else if(input == "stop"){
            c.stopServer();
            break;
        }
    
    //getSize
        else if(input == "size"){
            std::vector<int> dimensions = c.getScreenSize();
            std::cout << dimensions[0] << "x" << dimensions[1] << "\n";
        }
    //clearScreen
        else if(input == "clear"){
            c.clearScreen();
        }
    //scroll
        else if(input == "scroll"){
            scrolling = !scrolling;
        }
    
    //setPixel Coordinates Color
        else if(input == "set"){
            coordinates.push_back(coordinateInput());
            color = colorInput();
            
            c.setPixel(coordinates[0][0], coordinates[0][1], color[0], color[1], color[2]);
            coordinates.clear();
            color.clear();
        }
    //getPixel Coordinates
    
        else if(input == "get"){
            coordinates.push_back(coordinateInput());
            
            Client::ScreenPixel pixel = c.getPixel(coordinates[0][0], coordinates[0][1]);
            std::cout << (int)pixel.r << " " << (int)pixel.g << " " << (int)pixel.b << "\n";
            coordinates.clear();
        }
        
    //drawShape
        //Line Coordinates Coordinates Color
        else if(input == "line"){
            coordinates.push_back(coordinateInput());
            coordinates.push_back(coordinateInput());
            color = colorInput();
            
            c.drawLine(coordinates[0][0], coordinates[0][1], coordinates[1][0], coordinates[1][1], color[0], color[1], color[2]);
            coordinates.clear();
            color.clear();
        }
        //Polygon Coordinates[] Color
        else if(input == "poly"){
            std::cout << "Add points. Type \"next\" to finish\n";
            do{
                coordinates.push_back(coordinateInput());
                getline(std::cin, input);
            } while (input != "next");
            
            color = colorInput();
            
            c.drawPolygon(coordinates, color[0], color[1], color[2]);
            coordinates.clear();
            color.clear();
        }
        //Circle Coordinates Radius Color
        else if(input == "circle"){
            coordinates.push_back(coordinateInput());

            int radius = std::stoi(getInput("Radius"));
            color = colorInput();
            
            c.drawCircle(coordinates[0][0], coordinates[0][1], radius, color[0], color[1], color[2]);
            coordinates.clear();
            color.clear();
        }
        //Triangle Coordinates Coordinates Coordinates Color
        else if(input == "triangle"){
            coordinates.push_back(coordinateInput());
            coordinates.push_back(coordinateInput());
            coordinates.push_back(coordinateInput());
            color = colorInput();
            
            c.drawTriangle(coordinates[0][0], coordinates[0][1], coordinates[1][0], coordinates[1][1], coordinates[2][0], coordinates[2][1],color[0], color[1], color[2]);
            coordinates.clear();
            color.clear();
        }
        //Tetragon Coordinates Coordinates Coordinates Coordinates Color
        else if(input == "quad"){
            coordinates.push_back(coordinateInput());
            coordinates.push_back(coordinateInput());
            coordinates.push_back(coordinateInput());
            coordinates.push_back(coordinateInput());
            color = colorInput();
            
            c.drawTetragon(coordinates[0][0], coordinates[0][1], coordinates[1][0], coordinates[1][1], coordinates[2][0], coordinates[2][1], coordinates[3][0], coordinates[3][1],color[0], color[1], color[2]);
            coordinates.clear();
            color.clear();
        }
    //printText Text Coordinates Color
    
        else if(input == "write"){
            std::string text = getInput("Text");
            coordinates.push_back(coordinateInput());
            color = colorInput();
            
            c.printText(std::vector<char>(text.begin(), text.end()), coordinates[0][0], coordinates[0][1], color[0], color[1], color[2]);
            coordinates.clear();
            color.clear();
        }
    //loadImage File Coordinates
        else if(input == "image"){
            std::string path = getInput("Path");
            coordinates.push_back(coordinateInput());
            
            std::vector<std::string> preprocessing;
            
            std::cout << "Preprocessing:\n Options: \"FIT\", \"FILL\", \"STRETCH\", \"AREA\"\n(enter \"help\" for info, \"next\" to finish)\n";
            do{
                getline(std::cin, input);
                if((input == "help") ||
                (input == "help") ||
                (input == "help")){
                    std::cout << "FIT\tImage is rescaled so that the area is entirely filled it fits entirely into the specified area.\n";
                    std::cout << "FILL\tImage is rescaled so that the specified area is entirely filled.\n";
                    std::cout << "STRETCH\tImage is distorted so that the specified area is entirely filled\n";
                    std::cout << "AREA\tAn area is cut out of the image. If the area is larger than the image or includes pixels \"outside\" the image, empty pixels are filled with a color.\n";
                    std::cout << "\n";
                } else if (input == "FIT"){
                    
                    int width = std::stoi(getInput("Width"));
                    int height = std::stoi(getInput("Height"));
                    
                    preprocessing.push_back(preprocessing::fit(width, height));
                } else if (input == "FILL"){
                    
                    int width = std::stoi(getInput("Width"));
                    int height = std::stoi(getInput("Height"));
                    
                    preprocessing.push_back(preprocessing::fill(width, height));
                } else if (input == "STRETCH"){

                    int width = std::stoi(getInput("Width"));
                    int height = std::stoi(getInput("Height"));
                    
                    preprocessing.push_back(preprocessing::stretch(width, height));
                } else if (input == "AREA"){

                    std::vector<int> offset = coordinateInput();
                    int width = std::stoi(getInput("Width"));
                    int height = std::stoi(getInput("Height"));
                    color = colorInput();
                    preprocessing.push_back(preprocessing::area(offset[0], offset[1], width, height, color[0], color[1], color[2]));
                    color.clear();
                } else{
                    std::cout << "Invalid option\n";
                }
            } while(input != "next");
            
            c.loadImage(path, coordinates[0][0], coordinates[0][1], preprocessing);
            coordinates.clear();
        }
        
        c.updateScreen(scrolling);
    }
    
    return 0;
}

std::vector<int> coordinateInput(){
    std::vector<int> result;
    std::cout << "Enter coordinates\n";

    result.push_back(std::stoi(getInput("x")));
    

    result.push_back(std::stoi(getInput("y")));
    return result;
}
std::vector<uint8_t> colorInput(){
    std::vector<uint8_t> result;
    std::cout << "Enter color values\n";
    

    result.push_back((uint8_t)std::stoi(getInput("r")));

    result.push_back((uint8_t)std::stoi(getInput("g")));
    
    result.push_back((uint8_t)std::stoi(getInput("b")));
    return result;
}

std::string getInput(std::string prompt){
    std::string input;
    
    std::cout << prompt << ":";
    getline(std::cin, input);
    
    return input;
}
