/*
Copyright (C) 2022  Michel Macke, Finn Wundram, Hassene Jeddi, Mohamed Salah Messai

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "Client.hpp"

#include "../sharedlib/protocol.hpp"
#include "../sharedlib/base64.h"

#include <arpa/inet.h>

std::string Client::printText(std::vector<char> text, int x, int y, unsigned char red, unsigned char green,unsigned char blue)

{

    std::string temptext(text.begin(),text.end());
    
    std::string content;
    content += dcs_protocol::writeCoord( std::vector<int>( {x,y} ) );
    content += dcs_protocol::writeColor( std::vector<uint8_t>( {red, green, blue} ) );
    content += dcs_protocol::setStringAttribute("Text", temptext);
    
    std::string sendString = dcs_protocol::addHeader("printText", "Text", content);
    
    send_message(std::vector<uint8_t>(sendString.begin(), sendString.end()));
    
    
    log->info("Client: Message sent. Waiting for response\n"); 
    
    receive_message();
    log->info("Client: Received server response\n");                      //Auf Rueckmeldung warten
    return sendString;
}


std::ostringstream Client::setPixel(int x, int y, uint8_t red,uint8_t green,uint8_t blue){
    std::string content = dcs_protocol::writeCoord(
        std::vector<int>( {x, y} )
    );
    
    content += dcs_protocol::writeColor(
        std::vector<uint8_t>( {red, green, blue} )
    );
    
    std::string sendString = dcs_protocol::addHeader("setPixel", "Text", content);
    
        send_message(std::vector<uint8_t>(sendString.begin(), sendString.end()));
        log->info(sendString + "\n");
        log->info("Client: Message sent. Waiting for response\n"); 
        receive_message();
        log->info("Client: Received server response\n");                      //Auf Rueckmeldung warten
        
        std::ostringstream temp;
        temp << sendString;
        return temp;
}

std::vector<uint8_t> Client::get_bytes( std::string file_name )
{
    std::ifstream infile(file_name, std::ios::in | std::ios::binary);
    
    if (!infile) {
        log->error("file could not be opened\n");
        std::vector<uint8_t> empty;
        return empty;
    }
    
    std::istreambuf_iterator<char> start(infile), end;
    std::vector<uint8_t> data(start,end);
    
    return data;
}

std::string Client::loadImage(std::string filename, int xStart, int yStart, std::vector<std::string> image_options){
    auto bufferbyte = get_bytes(filename);
    std::string encodedFile = base64_encode(bufferbyte.data(), bufferbyte.size());

    std::vector<int> points({xStart, yStart});
    
    std::ostringstream message;
    
    message << dcs_protocol::writeCoord(points);
    message << dcs_protocol::setNumericAttribute("Preprocessing", image_options.size());
    
    for(std::vector<std::string>::iterator iter = image_options.begin(); iter < image_options.end(); iter++){
        message << dcs_protocol::setStringAttribute("Step", *iter);
    }
    
    message << dcs_protocol::setNumericAttribute("OriginalSize", bufferbyte.size());
    message << dcs_protocol::setNumericAttribute("EncodedSize", encodedFile.size());
 
    message << dcs_protocol::setStringAttribute("EncodedBytes", encodedFile);
    
    std::string image_file = dcs_protocol::addHeader("loadImage", "Image", message.str());

    send_message(std::vector<uint8_t>(image_file.begin(), image_file.end()));
    
    std::vector<uint8_t> response = receive_message();
    return std::string(response.begin(), response.end());
}

void Client::updateScreen(bool scrolling)
{
    std::string content = dcs_protocol::setBooleanAttribute("Scrolling", scrolling);
    std::string sendString = dcs_protocol::addHeader("updateScreen", "Text", content);
    
    send_message(std::vector<uint8_t>(sendString.begin(), sendString.end()));

    receive_message();
    log->info("Client: Received server response\n");           
}

void Client::clearScreen()
{
    std::string empty;
    
    std::string sendString = dcs_protocol::addHeader("clearScreen", "Text", empty);

    send_message(std::vector<uint8_t>(sendString.begin(), sendString.end()));

    receive_message();
    log->info("Client: Received server response\n");           
}

void Client::stopServer(){
    std::string empty;
    
    std::string sendString = dcs_protocol::addHeader("stopServer", "Text", empty);

    send_message(std::vector<uint8_t>(sendString.begin(), sendString.end()));

    receive_message();
    log->info("Client: Received server response\n");
}

std::vector<int> Client::getScreenSize()
{
    std::string empty;
    std::string sendString = dcs_protocol::addHeader("getSize", "Text", empty);
    

    send_message(std::vector<uint8_t>(sendString.begin(), sendString.end()));
    log->info(sendString + "\n");
    log->info("Client: Message sent. Waiting for response\n");
    
    std::vector<uint8_t> response = receive_message();
    log->info("Client: Received server response\n");                      //Auf Rueckmeldung warten
    
    std::string content (response.begin(), response.end());
    content = dcs_protocol::getContent(content);
    
    std::stringstream f;
    f.write((char*) content.data(), content.size());
    std::string line;
    
    std::getline(f, line);
    int width = dcs_protocol::getNumericAttribute(line);
    
    std::getline(f, line);
    int height = dcs_protocol::getNumericAttribute(line);
     
    return std::vector<int>( {width, height} );
}

Client::ScreenPixel Client::getPixel(int x, int y)
{
    
    std::string content = dcs_protocol::writeCoord(std::vector<int>({x, y}));
    std::string sendString = dcs_protocol::addHeader("getPixel", "Text", content);
    

    send_message(std::vector<uint8_t>(sendString.begin(), sendString.end()));
    log->info(sendString + "\n");
    log->info("Client: Message sent. Waiting for response\n");
    
    std::vector<uint8_t> response = receive_message();
    log->info("Client: Received server response\n");                      //Auf Rueckmeldung warten
    
    std::string rcvd_content (response.begin(), response.end());
    rcvd_content = dcs_protocol::getContent(rcvd_content);
    
    std::stringstream f;
    f.write((char*) rcvd_content.data(), rcvd_content.size());
    std::string line;
    
    std::getline(f, line);
    std::vector<uint8_t> color = dcs_protocol::readColor(line);
    
    Client::ScreenPixel sp;
    sp.r = color[0];
    sp.g = color[1];
    sp.b = color[2];
    
    return sp;

}

Client::Client(std::string file_name, Logger log_)
{
    log = log_;
    server_response_buffer = std::vector<char>(256);
    //Laden von Daten aus den Toml Dateien   

    auto serverConfig = toml::parse_file(file_name);

    int port = serverConfig["Port"].value_or(25565);

    server_input_socket = socket(AF_INET, SOCK_DGRAM, 0);

    server_address.sin_family = AF_INET;                               //Zieladresse angeben
    server_address.sin_port = htons(port);
    server_address.sin_addr.s_addr = INADDR_ANY;
    server_address_length = sizeof(server_address);
}

Client::Client(std::string ip_address, int port, Logger log_){

    log = log_;
    server_response_buffer = std::vector<char>(256);
    
    server_input_socket = socket(AF_INET, SOCK_DGRAM, 0);

    server_address.sin_family = AF_INET;                               //Zieladresse angeben
    server_address.sin_port = htons(port);
    
    if(inet_pton(AF_INET, ip_address.c_str(), &server_address.sin_addr) <= 0){
        log->error("Invalid address: " + ip_address);
        std::exit(EXIT_FAILURE);
    }
    server_address_length = sizeof(server_address);
}


std::string Client::drawGenericShape(std::string shape, std::vector<std::vector<int>> points, uint8_t r, uint8_t g, uint8_t b){
    std::ostringstream temp;
    
    temp << dcs_protocol::setStringAttribute("Shape", shape);
    temp << dcs_protocol::writeColor(std::vector<uint8_t>({r,g,b}));
    std::vector<std::string> pointsEnc;
    
    temp << dcs_protocol::setNumericAttribute("Points", points.size());
    for(std::vector<std::vector<int>>::iterator iter = points.begin();
        iter < points.end();
        iter++){
        temp << dcs_protocol::writeCoord(*iter);
    }
    
    std::string sendString = temp.str();
    
    sendString = dcs_protocol::addHeader("drawShape", CTYPESHAPE, sendString);
    
    return sendString;
}

std::string Client::drawLine(int xStart, int yStart, int xEnd, int yEnd, uint8_t r, uint8_t g, uint8_t b){

    std::vector<std::vector<int>> points;
    points.push_back(std::vector<int>({xStart, yStart}));
    points.push_back(std::vector<int>({xEnd, yEnd}));
    
    std::string sendString = drawGenericShape(LINE, points, r, g, b);
    
    send_message(std::vector<uint8_t>(sendString.begin(), sendString.end()));
    
    receive_message();
    log->info("Client: Received server response\n");                      //Auf Rueckmeldung warten
    
    return sendString;
}

std::string Client::drawPolygon(std::vector<std::vector<int>> points, uint8_t r, uint8_t g, uint8_t b){
    std::string sendString = drawGenericShape(POLYGON, points, r, g, b);
    
    send_message(std::vector<uint8_t>(sendString.begin(), sendString.end()));
    
    receive_message();
    log->info("Client: Received server response\n");                      //Auf Rueckmeldung warten
    
    return sendString;
}

std::string Client::drawTriangle(int xA, int yA, int xB, int yB, int xC, int yC, uint8_t r, uint8_t g, uint8_t b){
    
    std::vector<std::vector<int>> points;
    points.push_back(std::vector<int>({xA, yA}));
    points.push_back(std::vector<int>({xB, yB}));
    points.push_back(std::vector<int>({xC, yC}));
    

    std::string sendString = drawGenericShape(TRIANGLE, points, r, g, b);
    
    send_message(std::vector<uint8_t>(sendString.begin(), sendString.end()));
    
    receive_message();
    log->info("Client: Received server response\n");                      //Auf Rueckmeldung warten
    
    return sendString;
}

std::string Client::drawTetragon(int xA, int yA, int xB, int yB, int xC, int yC, int xD, int yD, uint8_t r, uint8_t g, uint8_t b){

    std::vector<std::vector<int>> points;
    points.push_back(std::vector<int>({xA, yA}));
    points.push_back(std::vector<int>({xB, yB}));
    points.push_back(std::vector<int>({xC, yC}));
    points.push_back(std::vector<int>({xD, yD}));
    
    std::string sendString = drawGenericShape(TETRAGON, points, r, g, b);
    
    send_message(std::vector<uint8_t>(sendString.begin(), sendString.end()));
    
    receive_message();
    log->info("Client: Received server response\n");                      //Auf Rueckmeldung warten
    
    return sendString;
}

std::string Client::drawCircle(int x, int y, int rad, uint8_t r, uint8_t g, uint8_t b){

    std::vector<std::vector<int>> points;
    points.push_back(std::vector<int>({x, y}));
    
    
    std::string sendString = drawGenericShape(CIRCLE, points, r, g, b);
    sendString += dcs_protocol::setNumericAttribute("Radius", rad);
    
    send_message(std::vector<uint8_t>(sendString.begin(), sendString.end()));
    
    receive_message();
    log->info("Client: Received server response\n");                      //Auf Rueckmeldung warten
    
    return sendString;
}

int Client::send_message(std::vector<uint8_t> msg){
    log->info("Message size: " + std::to_string(msg.size()));
    return sendto(server_input_socket, msg.data(), msg.size(), 0, (sockaddr *)&server_address, sizeof(server_address));
}

std::vector<uint8_t> Client::receive_message(){
    int server_response_status = recvfrom(server_input_socket, server_response_buffer.data(), server_response_buffer.size(), 0, (sockaddr*) &server_address, (socklen_t*) &server_address_length);
    
    std::vector<uint8_t> result;
    
    if(server_response_status > 0){
        result = std::vector<uint8_t>(server_response_buffer.begin(), server_response_buffer.begin() + server_response_status);
    }
    
    return result;
}
