/*
Copyright (C) 2022  Michel Macke, Finn Wundram, Hassene Jeddi, Mohamed Salah Messai

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include <stdlib.h>
#include <stdio.h>

#include <string> 


#include "toml.hpp"


#include <vector>
#include <string>

#include "Client.hpp"
bool unittest(std::string serverpath);

#include "LoggingFacility.hpp"
#include "SimpleFileLogger.hpp"

#include <iostream>
using std::cout;
using std::cerr;
using std::endl;

#include <fstream>
using std::ios;
using std::ifstream;

#include <exception>
using std::exception;

#include <cstring>
#include <cstdlib>
using std::exit;
using std::memcmp;


void testCode()
{
    if(unittest("../Server/"))
        std::cout << "Client and Server work as Expected";
    else
        std::cout << "Something broke along the way";
}

int main(int argc, char* argv[]){
    
         

    /*
    Logger log = std::make_shared<SimpleFileLogger>("client_log.txt");
    Client c = Client("./res/server.toml", log);
    
    //std::vector<int> screen = c.getScreenSize();
    //std::cout << screen[0] << std::endl;
    //std::cout << screen[1] << std::endl;
    
    const std::string file_name = "./test.ppm" ; // this file
    c.loadImage("./test.ppm");
    std::string test = "Demo Text";
    std::vector<char> testStr = std::vector<char>(test.begin(), test.end());
    c.printText(testStr, 70,0,255,0,255);*/
    if(unittest("../Server/"))
        std::cout << "Same";
    else
        std::cout << "NotSame";
    return 0;
    
}


bool equalFiles(ifstream& in1, ifstream& in2)
{
    ifstream::pos_type size1, size2;

    size1 = in1.seekg(0, ifstream::end).tellg();
    in1.seekg(0, ifstream::beg);

    size2 = in2.seekg(0, ifstream::end).tellg();
    in2.seekg(0, ifstream::beg);

    if(size1 != size2)
        return false;

    static const size_t BLOCKSIZE = 4096;
    size_t remaining = size1;

    while(remaining)
    {
        char buffer1[BLOCKSIZE], buffer2[BLOCKSIZE];
        size_t size = std::min(BLOCKSIZE, remaining);

        in1.read(buffer1, size);
        in2.read(buffer2, size);

        if(0 != memcmp(buffer1, buffer2, size))
            return false;

        remaining -= size;
    }

    return true;
}


bool unittest(std::string serverpath)
{
    Logger log = std::make_shared<SimpleFileLogger>("client_log.txt");
    Client c = Client("./res/server.toml", log);

    c.setPixel(0,0,0,125,255);
    Client::ScreenPixel px = c.getPixel(0,0);
    if(px.r != 0 || px.g != 125 || px.b !=255)
    {
        return false;
    }
    c.loadImage("./unit1.ppm");
    std::vector<char> text = {'t','e','s','t'};
    c.printText(text, 1,0,255,125,0);
    ifstream in1("./unittest.ppm", ios::binary);
    ifstream in2(serverpath + "./Bild.ppm", ios::binary);
    if(equalFiles(in1, in2))
    {
        return true;
    }
    return false;
}
