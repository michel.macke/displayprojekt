#ifndef DCS_PREPROCESSINGSTRINGS
#define DCS_PREPROCESSINGSTRINGS

#include <vector>
#include <stdint.h>
#include <string>
namespace preprocessing{

    std::string fit(int width, int height);
    
    std::string fill(int width, int height);
    
    std::string stretch(int width, int height);
    
    std::string area(int x, int y, int width, int height, uint8_t r, uint8_t g, uint8_t b);
}
#endif
