#include "PreProcessingStrings.hpp"

std::string preprocessing::fit(int width, int height){
    return "FIT " + std::to_string(width) + " " + std::to_string(height);
}

std::string preprocessing::fill(int width, int height){
    return "FILL " + std::to_string(width) + " " + std::to_string(height);
}

std::string preprocessing::stretch(int width, int height){
    return "STRETCH " + std::to_string(width) + " " + std::to_string(height);
}

std::string preprocessing::area(int x, int y, int width, int height, uint8_t r, uint8_t g, uint8_t b){
    return "FIT " + std::to_string(x) + " " + std::to_string(y) + " " + std::to_string(width) + " " + std::to_string(height) + " " + std::to_string((int) r) + " " + std::to_string((int) g) + " " + std::to_string((int) b);
}
