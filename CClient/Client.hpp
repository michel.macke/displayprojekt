/*
Copyright (C) 2022  Michel Macke, Finn Wundram, Hassene Jeddi, Mohamed Salah Messai

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string> 
#include "toml.hpp"
#include <netinet/in.h>
#include <vector>
#include <string>
#include <iostream>
#include <time.h>

#include "LoggingFacility.hpp"

typedef int SOCKET;

class Client{
    private:
        SOCKET server_input_socket;
        struct sockaddr_in server_address;
        int server_address_length;
        std::vector<char> server_response_buffer;
        int send_message(std::vector<uint8_t> msg);
        std::vector<uint8_t> receive_message();
        
        std::vector<uint8_t> get_bytes( std::string file_name );
        
        std::string drawGenericShape(std::string shape, std::vector<std::vector<int>> points, uint8_t r, uint8_t g, uint8_t b);
        
        Logger log;

    public:
        struct ScreenPixel{
            uint8_t r;
            uint8_t g;
            uint8_t b;
        };
        Client(std::string file_name, Logger log_);
        Client(std::string ip_address, int port, Logger log_);

        std::string printText(std::vector<char> text, int x, int y, unsigned char red, unsigned char green,unsigned char blue);
        std::ostringstream setPixel(int x, int y, uint8_t red,uint8_t green,uint8_t blue);
        
        //Shapes
        std::string drawLine(int xStart, int yStart, int xEnd, int yEnd, uint8_t r, uint8_t g, uint8_t b);
        std::string drawPolygon(std::vector<std::vector<int>> points, uint8_t r, uint8_t g, uint8_t b);
        std::string drawTriangle(int xA, int yA, int xB, int yB, int xC, int yC, uint8_t r, uint8_t g, uint8_t b);
        std::string drawTetragon(int xA, int yA, int xB, int yB, int xC, int yC, int xD, int yD, uint8_t r, uint8_t g, uint8_t b);
        std::string drawCircle(int x, int y, int rad, uint8_t r, uint8_t g, uint8_t b);
        
        
        std::vector<int> getScreenSize();
        std::string loadImage(std::string filename, int xStart = 0, int yStart = 0, std::vector<std::string> image_options = std::vector<std::string>(0));

        ScreenPixel getPixel(int x, int y);
        void updateScreen(bool scrolling);
        void clearScreen();
        void stopServer();
};
