#!/usr/bin/env bash
apt-get update --yes
apt-get upgrade --yes
apt-get install --yes cmake
apt-get install --yes make
apt-get install --yes g++
apt-get install --yes imagemagick
apt-get install --yes libgraphicsmagick++-q16-12
apt-get install --yes libmagick++-dev
apt-get install --yes maven
ldconfig /usr/local/lib
