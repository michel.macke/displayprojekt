/*
Copyright (C) 2022  Michel Macke, Finn Wundram, Hassene Jeddi, Mohamed Salah Messai

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef WETELCO_SP_H
#define WETELCO_SP_H

//Header file for this projects implementation of the WeTelCo Simple Protocol

#include <stdint.h>
#include <string>
#include <vector>
#include <array>

#define BROADCAST_TO_ALL_DISPLAYS = 0x00;
std::vector<uint8_t> embed_into_protocol_frame(std::vector<uint8_t> message, uint8_t target_display_id);



//write commands--------------------------------------------------------

//fade_in_effect
#define CLEAR_AND_REPLACE 0x00
#define OVERLAY 0x01
#define SCROLL_IN 0x02

//duration_of_visibility
#define SKIP_IMMEDIATELY 0x00


//text_alignment
#define LEFT_ALIGNED 0x00
#define CENTERED 0x01
#define RIGHT_ALIGNED 0x02

//text_font
#define FONT_6X8 0x01
#define FONT_12X16 0x02
#define FONT_16X24 0x03
#define FONT_6X7 0x04
#define FONT_24X32 0x05
#define FONT_10X16 0x06
#define FONT_6X8_EXT 0x07
#define SYMBOLFONT_01 0x08


std::vector<uint8_t> send_text(std::string page_number, uint8_t fade_in_effect, uint8_t fade_in_speed, uint8_t duration_of_visibility, uint8_t text_alignment, uint8_t text_color, uint8_t text_font, std::string text_content);


#define BYTES_PER_DISPLAY_ROW (256/8)
std::vector<uint8_t> send_image(std::string page_number, uint8_t fade_in_effect, uint8_t fade_in_speed, uint8_t duration_of_visibility, std::array<std::array<uint8_t, 32>, 32> bitmap_red, std::array<std::array<uint8_t, 32>, 32> bitmap_green, std::array<std::array<uint8_t, 32>, 32> bitmap_blue);



//-----------------------------------------------------------------------


#define ENDLESS_REPEATS 0x00
std::vector<uint8_t> send_playlist(uint8_t repetitions, string page_list);
std::vector<uint8_t> delete_playlist();
std::vector<uint8_t> delete_page(std::string page_list);
std::vector<uint8_t> show_page(std::string page_number);





//------------------------------------------------------------------------
#define DISPLAY_ACK 0x06
#define DISPLAY_NACK 0x15




#endif // WETELCO_SP_H
