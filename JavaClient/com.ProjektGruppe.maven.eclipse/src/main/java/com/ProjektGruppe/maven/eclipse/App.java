/*
Copyright (C) 2022  Michel Macke, Finn Wundram, Hassene Jeddi, Mohamed Salah Messai

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package com.ProjektGruppe.maven.eclipse;

import java.io.File;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.lang.Math;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;


import com.moandjiezana.toml.Toml;


public class App {

	private final int serverPort;
	

	public App(int serverPort) {
		this.serverPort = serverPort;
	}

	ArrayList<Integer> getScreenSize()
	{
		String sendString = Protocol.addHeader("getSize", "Text", "");
		
		sendMsg(sendString);
		
		String content = recieveMsg();
		content = Protocol.getContent(content);
		String[] lines = content.split("\n");
		
		int width = Protocol.getNumericAttribute(lines[0]);
		int height = Protocol.getNumericAttribute(lines[1]);
		
		return new ArrayList<Integer>( 
			Arrays.asList(
				Integer.valueOf(width), 
				Integer.valueOf(height)
			) 
		);
	}
	
	ArrayList<Integer> getPixel(int x, int y)
	{
		List<Integer> point = new ArrayList<Integer>( 
			Arrays.asList(
				Integer.valueOf(x), 
				Integer.valueOf(y)
			) 
		);
		String content = Protocol.writeCoord(point);
		String sendString = Protocol.addHeader("getPixel", "Text", content);
		
		sendMsg(sendString);
		
		String rcvd_content = recieveMsg();
		rcvd_content = Protocol.getContent(rcvd_content);
		String[] lines = rcvd_content.split("\n");
		
		ArrayList<Integer> color = new ArrayList<Integer>(Protocol.readColor(lines[0]));
		
		return color;
	}
	
	String drawLine(int xStart, int yStart, int xEnd, int yEnd, int r, int g, int b) {
		
		List<List<Integer>> points = new ArrayList<List<Integer>>();
		
		points.add(new ArrayList<Integer>(
			Arrays.asList(
				Integer.valueOf(xStart), 
				Integer.valueOf(yStart)
			)
		));
		points.add(new ArrayList<Integer>(
			Arrays.asList(
				Integer.valueOf(xEnd), 
				Integer.valueOf(yEnd)
			)
		));
		
		String message = DrawShape.drawGenericShape("Line", points, r, g, b);
		
		sendMsg(message);
		
		String response = recieveMsg();
		return response;
	}
	
String drawPolygon(List<List<Integer>> points, int r, int g, int b) {
		
		String message = DrawShape.drawGenericShape("Polygon", points, r, g, b);
		
		sendMsg(message);
		
		String response = recieveMsg();
		return response;
	}

String drawTriangle(int xA, int yA, int xB, int yB, int xC, int yC, int r, int g, int b) {
	
	List<List<Integer>> points = new ArrayList<List<Integer>>();
	
	points.add(new ArrayList<Integer>(
		Arrays.asList(
			Integer.valueOf(xA), 
			Integer.valueOf(yA)
		)
	));
	points.add(new ArrayList<Integer>(
		Arrays.asList(
			Integer.valueOf(xB), 
			Integer.valueOf(yB)
		)
	));
	points.add(new ArrayList<Integer>(
		Arrays.asList(
			Integer.valueOf(xC), 
			Integer.valueOf(yC)
		)
	));
	
	String message = DrawShape.drawGenericShape("Triangle", points, r, g, b);
	
	sendMsg(message);
	
	String response = recieveMsg();
	return response;
}

String drawTetragon(int xA, int yA, int xB, int yB, int xC, int yC, int xD, int yD, int r, int g, int b) {
	
	List<List<Integer>> points = new ArrayList<List<Integer>>();
	
	points.add(new ArrayList<Integer>(
		Arrays.asList(
			Integer.valueOf(xA), 
			Integer.valueOf(yA)
		)
	));
	points.add(new ArrayList<Integer>(
		Arrays.asList(
			Integer.valueOf(xB), 
			Integer.valueOf(yB)
		)
	));
	points.add(new ArrayList<Integer>(
		Arrays.asList(
			Integer.valueOf(xC), 
			Integer.valueOf(yC)
		)
	));
	points.add(new ArrayList<Integer>(
		Arrays.asList(
			Integer.valueOf(xD), 
			Integer.valueOf(yD)
		)
	));
	
	String message = DrawShape.drawGenericShape("Tetragon", points, r, g, b);
	
	sendMsg(message);
	
	String response = recieveMsg();
	return response;
}

	String drawCircle(int x, int y, int radius, int r, int g, int b) {
	
		List<List<Integer>> points = new ArrayList<List<Integer>>();
	
		points.add(new ArrayList<Integer>(
			Arrays.asList(
				Integer.valueOf(x), 
				Integer.valueOf(y)
			)
		));
	
		String message = DrawShape.drawGenericShape("Circle", points, r, g, b);
		message += Protocol.setNumericAttribute("Radius", radius);
	
		sendMsg(message);
	
		String response = recieveMsg();
		return response;
	}
	
	String loadImage(String file_path, int xStart, int yStart, List<String> preprocessing) throws IOException {
		byte[] bufferbyte = Files.readAllBytes( Paths.get(file_path));
		String encodedFile = Base64.getEncoder().encodeToString(bufferbyte);
		
		List<Integer> points = new ArrayList<Integer>(
			Arrays.asList(
				Integer.valueOf(xStart), 
				Integer.valueOf(yStart)
			)
		);
    
		String message = "";
    
		message += Protocol.writeCoord(points);
		message += Protocol.setNumericAttribute("Preprocessing", preprocessing.size());
		for(String step: preprocessing){
			message += Protocol.setStringAttribute("Step", step);
		}
		message += Protocol.setNumericAttribute("OriginalSize", bufferbyte.length);
		message += Protocol.setNumericAttribute("EncodedSize", encodedFile.length());
 
		message += Protocol.setStringAttribute("EncodedBytes", encodedFile);
    
		String sendStr = Protocol.addHeader("loadImage", "Image", message);
    		sendMsg(sendStr);
    		
		return recieveMsg();
	}
	
	public void setPixel(int x, int y, byte r, byte g, byte b){
		String content = "";
		
		List<Integer> point = new ArrayList<Integer>(
			Arrays.asList(
				Integer.valueOf(x), 
				Integer.valueOf(y)
			)
		);
		content += Protocol.writeCoord( point );
		
		List<Integer> color = new ArrayList<Integer>(
			Arrays.asList(
				Integer.valueOf(r), 
				Integer.valueOf(g), 
				Integer.valueOf(b)
			)
		);
		content += Protocol.writeColor( color );
		
		String sendString = Protocol.addHeader("setPixel", "Text", content);
		sendMsg(sendString);
		recieveMsg();

	}
	
	public String printText(String text, int x, int y,byte r,byte g, byte b){
		
		String content = "";
		
		List<Integer> point = new ArrayList<Integer>(
			Arrays.asList(
				Integer.valueOf(x), 
				Integer.valueOf(y)
			)
		);
		content += Protocol.writeCoord( point );
		
		List<Integer> color = new ArrayList<Integer>(
			Arrays.asList(
				Integer.valueOf(r), 
				Integer.valueOf(g), 
				Integer.valueOf(b)
			)
		);
		content += Protocol.writeColor( color );
		
		content += Protocol.setStringAttribute("Text", text);
		
		String sendString = Protocol.addHeader("printText", "Text", content);
		sendMsg(sendString);
		return recieveMsg();
	}
	
	public String updateScreen(boolean scrolling){
		String content = Protocol.setBooleanAttribute("Scrolling", scrolling);
		String message = Protocol.addHeader("updateScreen", "Text", content);
		sendMsg(message);
		
		return recieveMsg();
	}
	
	public void clearScreen(){
		String message = Protocol.addHeader("clearScreen", "Text", "");
		sendMsg(message);
		recieveMsg();
	}
	
	public void stopServer(){
		String message = Protocol.addHeader("stopServer", "Text", "");
		sendMsg(message);
		recieveMsg();
	}
	
	public void sendMsg(String msg) {
		/**
		 * Create a new server socket and bind it to a free port. I have chosen one in
		 * the 49152 - 65535 range, which are allocated for internal applications
		 */
		
		try (DatagramSocket serverSocket = new DatagramSocket(50000)) {
			DatagramPacket datagramPacket = new DatagramPacket(
					msg.getBytes(StandardCharsets.UTF_8),
					msg.length(),
					InetAddress.getLocalHost(),
					serverPort
			);

			serverSocket.send(datagramPacket);

			// The server will generate 3 messages and send them to the Server
			/*for (int i = 0; i < 3; i++) {
				String message = "Message number " + i;
				DatagramPacket datagramPacket = new DatagramPacket(message.getBytes(), message.length(),
						InetAddress.getLocalHost(), serverPort
				);
				serverSocket.send(datagramPacket);
			}*/
		} catch (SocketException e) {
			e.printStackTrace();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	public String recieveMsg() {

		/**
		 * Bind the client socket to the port on which you expect to read incoming
		 * messages
		 */

		try (DatagramSocket clientSocket = new DatagramSocket(50000)) {

			/**
			 * Create a byte array buffer to store incoming data. If the message length
			 * exceeds the length of your buffer, then the message will be truncated. To
			 * avoid this, you can simply instantiate the buffer with the maximum UDP packet
			 * size, which is 65506
			 */

			byte[] buffer = new byte[65507];

			// Set a timeout of 3000 ms for the client.

			clientSocket.setSoTimeout(3000);
			while (true) {
				DatagramPacket datagramPacket = new DatagramPacket(buffer, 0, buffer.length);
				/**
				 * The receive method will wait for 3000 ms for data. After that, the client
				 * will throw a timeout exception.
				 */
				clientSocket.receive(datagramPacket);
				String receivedMessage = new String(datagramPacket.getData());
				System.out.println(receivedMessage);
				return receivedMessage;
			}

		} catch (SocketException e) {
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("Timeout. Client is closing.");
		}
		return null;
	}

	public static void main(String[] args) {
		System.setProperty("file.encoding","UTF-8");
		Toml toml = new Toml().read(getTomlFile());

		App test = new App(Math.toIntExact(toml.getLong("Port")));// Server Port should be given in Terminal (e.g.: java App 35683)
		
		try{
			test.loadImage("../testt.png" ,-20, 10, new ArrayList<String>());
		} catch( Exception e){
			e.printStackTrace();
		}

		/*
		int red=255;
		byte b = (byte) red;
		int red2 = b & 0xFF;
		int green= b & 0xFF;
		int blue= b & 0xFF;
		byte rbyte = (byte)254;
		String str_test=printText("HEllo World",0,0,(byte) red2,(byte)green,(byte)blue);
		test.setPixel(0, 10, (byte)rbyte, (byte)green, (byte)blue);
		ArrayList<Integer> pixelInfo = test.getPixel(0, 10);
		//test.sendMsg(str_test);

		ArrayList<Integer> result = test.getScreenSize();
		System.out.println(pixelInfo.get(0));
		System.out.println(pixelInfo.get(1));
		System.out.println(pixelInfo.get(2));
		*/
	}

	public static File getTomlFile()
	{
		return new File("./res/server.toml");
	}
}
