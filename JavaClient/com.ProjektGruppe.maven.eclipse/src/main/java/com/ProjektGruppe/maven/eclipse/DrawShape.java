/*
Copyright (C) 2022  Michel Macke, Finn Wundram, Hassene Jeddi, Mohamed Salah Messai

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package com.ProjektGruppe.maven.eclipse;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DrawShape {
	private DrawShape() {}
	
    public static String drawGenericShape(String shape, List<List<Integer>> points, int r, int g, int b) {
    	String output = Protocol.setStringAttribute("Shape", shape);
    	output += Protocol.writeColor(new ArrayList<Integer>(
    										Arrays.asList(
    												Integer.valueOf(r),
    												Integer.valueOf(g),
    												Integer.valueOf(b)
    												)));
    	output += Protocol.setNumericAttribute("Points", points.size());
    	for(List<Integer> point: points) {
    		output += Protocol.writeCoord(point);
    	}
    	
    	return Protocol.addHeader("drawShape", "Shape", output);
    }
}
