/*
Copyright (C) 2022  Michel Macke, Finn Wundram, Hassene Jeddi, Mohamed Salah Messai

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package com.ProjektGruppe.maven.eclipse;

public class PreProcessingStrings{
	public static String fit(int width, int height){
		return "FIT " + width + " " + height;
	}
	
	public static String fill(int width, int height){
		return "FILL " + width + " " + height;
	}
	
	public static String stretch(int width, int height){
		return "STRETCH " + width + " " + height;
	}
	
	public static String area(int x, int y, int width, int height, int r, int g, int b){
		return "AREA " + x + " " + y + " " + width + " " + height + " " + r + " " + g + " " + b;
	}
}
