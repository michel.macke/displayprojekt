/*
Copyright (C) 2022  Michel Macke, Finn Wundram, Hassene Jeddi, Mohamed Salah Messai

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package com.ProjektGruppe.maven.eclipse;

import java.util.List;
import java.util.ArrayList;

public class Protocol {
	private Protocol() {}
	
    public static String setNumericAttribute(String name, int number) {
    	return name + ": " + number + "\n";
    }
    
    public static int getNumericAttribute(String line) {
    	String numStr = line.split(" ", 2)[1];
    	numStr = numStr.split("\n")[0];
    	return Integer.parseInt(numStr);
    }
    
    public static String setBooleanAttribute(String name, boolean value){
        if(value){
        	return setStringAttribute(name, "True");
        } else {
        	return setStringAttribute(name, "False");
        }
    }
    
    public static boolean getBooleanAttribute(String line) {
    	String boolStr = getStringAttribute(line);
    	return boolStr.equals("True");
    }
    
    public static String setStringAttribute(String name, String value) {
    	return name + ": " + value + "\n";
    }
    
    public static String getStringAttribute(String line) {
    	String numStr = line.split(" ", 2)[1];
    	if(numStr.charAt(numStr.length()-1) == '\n') {
    		numStr = numStr.substring(0, numStr.length() - 1);
    	}
    	return numStr;
    }
    
    
    public static String getAttributeName(String line) {
    	return line.split(":", 2)[0];
    }
    
    public static String addHeader(String function, String contentType, String content) {
    	String message = "";
    	
    	message += setStringAttribute("Function", function);
    	message += setStringAttribute("ContentType", contentType);
    	message += setNumericAttribute("ContentLength", content.length());
    	message += "\n";
    	message += content;
    	
    	return message;
    }
    
    public static String getFunctionFromHeader(String msg) {
    	return getStringAttribute(msg.split("\n", 5)[0]);
    }
    
    public static String getContentTypeFromHeader(String msg) {
    	return getStringAttribute(msg.split("\n", 5)[1]);
    }
    
    public static int getContentLengthFromHeader(String msg) {
    	return getNumericAttribute(msg.split("\n", 5)[2]);
    }
    
    public static String getContent(String msg) {
    	return msg.split("\n", 5)[4];
    }
    
    public static String writeColor(List<Integer> col) {
    	String message = "Color:";
    	
    	for(Integer val: col) {
    		val = Integer.max(0, val.intValue());
    		val = Integer.min(255, val.intValue());
    		message += " " + val.toString();
    	}
    	
    	message += "\n";
    	
    	return message;
    }
    
    public static List<Integer> readColor(String msg){
    	List<Integer> result = new ArrayList<Integer>();
    	
    	String line = msg.split(" ", 2)[1];
    	if(line.charAt(line.length()-1) == '\n') {
    		line = line.substring(0, line.length() - 1);
    	}
    	
    	String[] numStrs = line.split(" ");
    	
    	for(String num: numStrs) {
    		result.add(Integer.valueOf(num));
    	}
    	
		return result;
    }
    
    public static String writeCoord(List<Integer> coord) {
    	String message = "Coord:";
    	
    	for(Integer val: coord) {
    		message += " " + val.toString();
    	}
    	
    	message += "\n";
    	
    	return message;
    }
    
    public static List<Integer> readCoord(String msg){
    	return readColor(msg);
    }
}
