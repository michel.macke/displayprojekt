package com.ProjektGruppe.maven.eclipse;

import com.ProjektGruppe.maven.eclipse.Protocol;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

public class ProtocolTest {
	@Test
    public void numericReadWrite()
    {
		int value = 255;
		String encoded = Protocol.setNumericAttribute("Num", value);
		
        assertEquals( value , Protocol.getNumericAttribute(encoded));
    }
	
	@Test
    public void stringReadWrite()
    {
		String value = "placeholder";
		String encoded = Protocol.setStringAttribute("Text", value);
		
        assertEquals( value , Protocol.getStringAttribute(encoded));
    }
    
    @Test
    public void boolReadWrite()
    {
		boolean value = true;
		String encoded = Protocol.setBooleanAttribute("Truth", value);
		
        assertEquals( value , Protocol.getBooleanAttribute(encoded));
    }
	
	@Test
    public void readAttributeName()
    {
		String nameStr = "Text";
		String encodedStr = Protocol.setStringAttribute(nameStr, "ph");
		
        assertEquals( nameStr , Protocol.getAttributeName(encodedStr));
        
        String nameNum = "Number";
		String encodedNum = Protocol.setNumericAttribute(nameNum, 255);
		
        assertEquals( nameNum , Protocol.getAttributeName(encodedNum));
    }
	
	@Test
	public void headerCanBeRead() {
		String content = "placeholder";
		
		String function = "printTest";
		String contentType = "Text";
		
		String message = Protocol.addHeader(function, contentType, content);
		
		assertEquals(function, Protocol.getFunctionFromHeader(message));
		assertEquals(contentType, Protocol.getContentTypeFromHeader(message));
		assertEquals(content.length(), Protocol.getContentLengthFromHeader(message));
		assertEquals(content, Protocol.getContent(message));
	}
	
	@Test
	public void colorReadWrite() {
		int r = 255;
		int g = 110;
		int b = 12;
		
		ArrayList<Integer> channels = new ArrayList<Integer>(
											Arrays.asList(
													Integer.valueOf(r), 
													Integer.valueOf(g), 
													Integer.valueOf(b)
													)
											);
		
		String encodedCol = Protocol.writeColor(channels);
		List<Integer> decodedCol = Protocol.readColor(encodedCol);
		
		assertEquals(channels.size(), decodedCol.size());
		assertEquals(r, decodedCol.get(0).intValue());
		assertEquals(channels.get(0), decodedCol.get(0));
		assertEquals(channels.get(1), decodedCol.get(1));
		assertEquals(channels.get(2), decodedCol.get(2));
	}
	
	@Test
	public void colorInByteRange() {
		int r = 2000;
		int g = 500;
		int b = 90;
		
		ArrayList<Integer> channels = new ArrayList<Integer>(
											Arrays.asList(
													Integer.valueOf(r), 
													Integer.valueOf(g), 
													Integer.valueOf(b)
													)
											);
		
		String encodedCol = Protocol.writeColor(channels);
		List<Integer> decodedCol = Protocol.readColor(encodedCol);
		
		assertEquals(channels.size(), decodedCol.size());
		assertNotEquals(r, decodedCol.get(0).intValue());
		assertEquals(Integer.valueOf(255), decodedCol.get(0));
		assertEquals(Integer.valueOf(255), decodedCol.get(1));
		assertEquals(channels.get(2), decodedCol.get(2));
	}
	
	@Test
	public void coordReadWrite() {
		int x = -20;
		int y = 100;
		
		ArrayList<Integer> axis = new ArrayList<Integer>(
											Arrays.asList(
													Integer.valueOf(x), 
													Integer.valueOf(y)
													)
											);
		
		String encodedCoord = Protocol.writeCoord(axis);
		List<Integer> decodedCoord = Protocol.readCoord(encodedCoord);
		
		assertEquals(axis.size(), decodedCoord.size());
		assertEquals(x, decodedCoord.get(0).intValue());
		assertEquals(axis.get(0), decodedCoord.get(0));
		assertEquals(axis.get(1), decodedCoord.get(1));
	}
}
