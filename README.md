# DisplayProjekt
Link zu der Gitlab Repository des Projekts: https://gitlab.gwdg.de/michel.macke/displayprojekt


Quellen: 
Toml++: für Toml parsing in C++(https://github.com/marzer/tomlplusplus)
Toml4J: Toml Parsing in Java (https://github.com/mwanji/toml4j)


Installation:
Client Rechner:
1. Auf den Client Rechnern einmalig 'sudo bash ./install.sh' ausführen.
2. 'chmod a+x compile.sh' ausführen
3. './compile.sh' ausführen um das Projet zu kompilieren
Bei jedem start des Raspberry Pi's:
raspberry.toml 

Server Rechner (Raspberry Pi)
1. IP Adresse des Raspberry Pi's überprüfen:
1.1'ip a s' eingeben, dann die ipv4 Adresse ablesen. Diese auf den Client Rechern in der (Server/res/)raspberry.toml eintragen
2. 'sudo python3 recieve2.py' ausführen um das Programm zu starten

