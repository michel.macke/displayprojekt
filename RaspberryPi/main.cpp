/*
Copyright (C) 2022  Michel Macke, Finn Wundram, Hassene Jeddi, Mohamed Salah Messai

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include <stdlib.h>
#include <stdio.h>

#include <sys/types.h>
#include <sys/socket.h>

#include <netinet/in.h>

#include <arpa/inet.h>

#include <vector>
#include <string>

#include <iostream>
#include <fstream>


typedef int SOCKET;


int main(int argc, char* argv[]){

 SOCKET server_input_socket;
 int server_input_port = 20001;

 std::vector<char> server_response_buffer(256);

 std::string filename(argv[1]);
 std::ifstream myfile ("./test.ppm");
 std::ifstream input( filename, std::ios::binary );

    // copies all data into buffer
 std::vector<unsigned char> buffer(std::istreambuf_iterator<char>(input), {});

 server_input_socket = socket(AF_INET, SOCK_DGRAM, 0);

 struct sockaddr_in server_address;
 server_address.sin_family = AF_INET;                               //Zieladresse angeben
 server_address.sin_port = htons(server_input_port);
 std::cout << buffer.size() << std::endl;
 server_address.sin_addr.s_addr = inet_addr("192.168.111.132");
 
 int server_address_length = sizeof(server_address);

 std::string test(buffer.begin(), buffer.end());

 sendto(server_input_socket, test.data(), test.size(), 0, (sockaddr*) &server_address, server_address_length);
 printf("Client: Message sent. Waiting for response\n");            //Nachricht an Server senden

 
 int server_response_status = recvfrom(server_input_socket, server_response_buffer.data(), server_response_buffer.size(), 0, (sockaddr*) &server_address, (socklen_t*) &server_address_length);
 printf("Client: Received server response\n");                      //Auf Rueckmeldung warten
 return 0;
}
