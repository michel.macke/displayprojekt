#Copyright (C) 2022  Michel Macke, Finn Wundram, Hassene Jeddi, Mohamed Salah Messai
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <https://www.gnu.org/licenses/>.


import subprocess
import time
import psutil
import socket
import os
import signal
import sys

def get_ip():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        # doesn't even have to be reachable
        s.connect(('10.255.255.255', 1))
        IP = s.getsockname()[0]
    except Exception:
        IP = '127.0.0.1'
    finally:
        s.close()
    return IP

def main():
    print("starting process")
    localIP = get_ip()
    image = "/home/pi/img.ppm"
    commands = "nice -n 20 /home/pi/rpi-rgb-led-matrix/examples-api-use/demo -t -1 -D 1 img.ppm -m 0 --led-chain=2 --led-rows=32 --led-cols=64"
    print(localIP)

    localPort = 20001

    bufferSize = 1024000

    

    msgFromServer = "Hello UDP Client"

    bytesToSend = str.encode(msgFromServer)

    # Create a datagram socket

    UDPServerSocket = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)

    # Bind to address and ip

    UDPServerSocket.bind((localIP, localPort))

    print("UDP server up and listening")

    # Listen for incoming datagrams
    rpiServer = subprocess.Popen(commands,shell=True, stdout=subprocess.DEVNULL, stdin=subprocess.DEVNULL, preexec_fn=os.setsid)
    try:
        while(True):

            bytesAddressPair = UDPServerSocket.recvfrom(bufferSize)
            os.killpg(os.getpgid(rpiServer.pid),signal.SIGTERM)
            
            message = bytesAddressPair[0]

            address = bytesAddressPair[1]

            clientMsg = message
            print(len(message))

            clientIP  = format(address)
            
            print(clientMsg)
            print("Writing base64 to file")
            with open(image, 'wb') as file:
                file.write(clientMsg)
            
            
            rpiServer = subprocess.Popen(commands,shell=True, stdout=subprocess.DEVNULL, stdin=subprocess.DEVNULL, preexec_fn=os.setsid)
           
            # Sending a reply to client
            UDPServerSocket.sendto(bytesToSend, address)


    except KeyboardInterrupt:
        print("Ending")
        os.killpg(os.getpgid(rpiServer.pid),signal.SIGTERM)
        sys.exit()
        
if __name__ == "__main__":
    main()
