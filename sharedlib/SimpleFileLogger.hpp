#ifndef DCS_SIMPLEFILELOGGER
#define DCS_SIMPLEFILELOGGER

#include "LoggingFacility.hpp"

class SimpleFileLogger : public LoggingFacility{
    private:
        std::string_view logFile;
        int print_to_message_log_file(const void* buf, size_t len);
    public:
        SimpleFileLogger(std::string_view logFile_);
        virtual void info(std::string_view entry) override;
        virtual void warning(std::string_view entry) override;
        virtual void error(std::string_view entry) override;
};
#endif
