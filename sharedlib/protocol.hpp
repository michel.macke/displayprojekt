#ifndef DCS_PROTOCOL
#define DCS_PROTOCOL


#include <string>
#include <vector>
#include <stdint.h>

namespace dcs_protocol{

//Functions
//#define FUNCTIONDRAWSHAPE "drawShape"

//ContentTypes
#define CTYPESHAPE "Shape"

//Shapes
#define LINE "Line"
#define POLYGON "Polygon"
#define TRIANGLE "Triangle"
#define TETRAGON "Tetragon"
#define CIRCLE "Circle"

std::string setNumericAttribute(std::string name, int number);
std::string setStringAttribute(std::string name, std::string val);
std::string setBooleanAttribute(std::string name, bool val);

int getNumericAttribute(std::string line);
std::string getStringAttribute(std::string line);
bool getBooleanAttribute(std::string line);
std::string getAttributeName(std::string line);

std::string addHeader(std::string function, std::string contentType, std::string content);

std::string writeColor(std::vector<uint8_t> col);
std::string writeCoord(std::vector<int> coord);

std::string getFunctionFromHeader(std::string msg);
std::string getContentTypeFromHeader(std::string msg);
unsigned int getContentLengthFromHeader(std::string msg);
std::string getContent(std::string msg);

std::vector<uint8_t> readColor(std::string col);
std::vector<int> readCoord(std::string coord);

}
#endif
