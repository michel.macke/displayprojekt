#include "protocol.hpp"

#include <sstream>

std::string dcs_protocol::setNumericAttribute(std::string name, int number){
    return name + ": " + std::to_string(number) + "\n";
}

std::string dcs_protocol::setStringAttribute(std::string name, std::string val){
    return name + ": " + val + "\n";
}

std::string dcs_protocol::setBooleanAttribute(std::string name, bool val){
    if(val){
        return setStringAttribute(name, "True");
    } else {
        return setStringAttribute(name, "False");
    }
}

int dcs_protocol::getNumericAttribute(std::string line){
    line.erase(0, line.find(" ") + 1);
    return std::stoi(line);
}
std::string dcs_protocol::getStringAttribute(std::string line){
    line.erase(0, line.find(" ") + 1);
    if(*(line.end()-1) == '\n'){
        line.pop_back();
    }
    return line;
}

bool dcs_protocol::getBooleanAttribute(std::string line){
    std::string value = getStringAttribute(line);
    if(value == "True"){
        return true;
    } else {
        return false;
    }
}

std::string dcs_protocol::getAttributeName(std::string line){
    return line.substr(0, line.find(":"));
}

std::string dcs_protocol::addHeader(std::string function, std::string contentType, std::string content){
    std::ostringstream temp;
    
    temp << setStringAttribute("Function", function);
    temp << setStringAttribute("ContentType", contentType);
    temp << setNumericAttribute("ContentLength", content.size());
    temp << "\n";
    temp << content;
    
    return temp.str();
}

std::string dcs_protocol::getFunctionFromHeader(std::string msg){
    std::stringstream temp;
    temp.write((char*) msg.data(), msg.size());
    
    std::string line;
    std::getline(temp, line);
    
    return getStringAttribute(line);
}

std::string dcs_protocol::getContentTypeFromHeader(std::string msg){
    std::stringstream temp;
    temp.write((char*) msg.data(), msg.size());
    
    std::string line;
    std::getline(temp, line);
    std::getline(temp, line);
    
    return getStringAttribute(line);
}

unsigned int dcs_protocol::getContentLengthFromHeader(std::string msg){
    std::stringstream temp;
    temp.write((char*) msg.data(), msg.size());
    
    std::string line;
    std::getline(temp, line);
    std::getline(temp, line);
    std::getline(temp, line);
    
    return getNumericAttribute(line);
}

std::string dcs_protocol::getContent(std::string msg){
    std::stringstream temp;
    temp.write((char*) msg.data(), msg.size());
    
    std::string line;
    
    std::getline(temp, line); //Function
    std::getline(temp, line); //ContentType
    std::getline(temp, line); //ContentLength
    std::getline(temp, line); // "\n"
    
    std::istreambuf_iterator<char> start(temp), end;
    std::string content(start, end);
    
    return content;
}

std::string dcs_protocol::writeColor(std::vector<uint8_t> col){
    std::ostringstream temp;
    temp << "Color:";
    
    for(std::vector<uint8_t>::iterator iter = col.begin();
        iter < col.end();
        iter++){
        temp << " " << (int) *iter;
    }
    temp << "\n";
    
    return temp.str();
}

std::string dcs_protocol::writeCoord(std::vector<int> coord){
    std::ostringstream temp;
    temp << "Coord:";
    
    for(std::vector<int>::iterator iter = coord.begin();
        iter < coord.end();
        iter++){
        temp << " " << *iter;
    }
    temp << "\n";
    
    return temp.str();
}

std::vector<uint8_t> dcs_protocol::readColor(std::string col){
    std::string line = col; 
    line.erase(0, line.find(" ") + 1);//"Color: "
    std::vector<uint8_t> color;
    
    //Werte der Farbkanäle durch Leerzeichen getrennt
    size_t split = 0;
    std::string val;
    while((split = line.find(" ")) != std::string::npos){
        val = line.substr(0, split);
        color.push_back(
            std::stoi(
                val
            )
        );
        
        line.erase(0, split + 1);
    }
    
    //Letzten Wert einfügen
    color.push_back(
        std::stoi(
            line
        )
    );
    
    return color;
}

std::vector<int> dcs_protocol::readCoord(std::string coord){
    std::string line = coord; 
    line.erase(0, line.find(" ") + 1);//"Coord: "
    std::vector<int> axis;
    
    //Werte der Axen durch Leerzeichen getrennt
    size_t split = 0;
    std::string val;
    while((split = line.find(" ")) != std::string::npos){
        val = line.substr(0, split);
        axis.push_back(
            std::stoi(
                val
            )
        );
        
        line.erase(0, split + 1);
    }
    
    //Letzten Wert einfügen
    axis.push_back(
        std::stoi(
            line
        )
    );
    
    return axis;
}
