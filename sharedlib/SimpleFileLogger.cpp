#include "SimpleFileLogger.hpp"


#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <string>

SimpleFileLogger::SimpleFileLogger(std::string_view logFile_){
    logFile = logFile_;
    
    int log_flags = O_RDWR | O_APPEND | O_CREAT | O_TRUNC;
    int log_mode = S_IRWXU;
    int log_file = open(logFile.data(), log_flags, log_mode);

    if(log_file < 0){
        perror("Server: Could not open testLog file\n");
        std::exit(EXIT_FAILURE);
    } else {
        close(log_file);
    }
}

void SimpleFileLogger::info(std::string_view entry){
    std::string output = "[INFO] " + std::string(entry.begin(), entry.end()) + "\n";
    print_to_message_log_file(output.data(), output.size());
}

void SimpleFileLogger::warning(std::string_view entry){
    std::string output = "[WARNING] " + std::string(entry.begin(), entry.end()) + "\n";
    print_to_message_log_file(output.data(), output.size());
}

void SimpleFileLogger::error(std::string_view entry){
    std::string output = "[ERROR] " + std::string(entry.begin(), entry.end()) + "\n";
    print_to_message_log_file(output.data(), output.size());
}

int SimpleFileLogger::print_to_message_log_file(const void* buf, size_t len){

   int log_flags = O_RDWR | O_APPEND | O_CREAT;
   int log_mode = S_IRWXU;
   int log_file = open(logFile.data(), log_flags, log_mode);

   if(log_file < 0){
      perror("SimpleFileLogger: Could not open log file\n");
      return -1;
   } else {
      write(log_file, buf,len);
      close(log_file);
   }
   return 0;
}
