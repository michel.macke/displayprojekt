#ifndef DCS_LOGGER
#define DCS_LOGGER
#include <memory>
#include <string_view>

class LoggingFacility{
    public:
        virtual ~LoggingFacility() = default;
        virtual void info(std::string_view entry) = 0;
        virtual void warning(std::string_view entry) = 0;
        virtual void error(std::string_view entry) = 0;
};

using Logger = std::shared_ptr<LoggingFacility>;
#endif
