#ifndef TEXTRENDERER_BITMAP_HPP
#define TEXTRENDERER_BITMAP_HPP

#include <string>
#include <vector>

/**
 * @author: Julian Hinxlage
 * @since: v0.0.0
 * @brief: Used to load a Bitmap from a file.
 */
class Bitmap {
public:
    Bitmap();
    explicit Bitmap(const std::string &file);

    /**
     * @author: Julian Hinxlage
     * @since: v0.0.0
     * @brief: Used to load a Bitmap from a file.
     */
    void load(const std::string &file);

    /**
     * @author: Julian Hinxlage
     * @since: v0.0.0
     * @return: the width of the image.
     */
    int getWidth();

    /**
     * @author: Julian Hinxlage
     * @since: v0.0.0
     * @return: the height of the image
     */
    int getHeight();

    /**
     * @author: Julian Hinxlage
     * @since: v0.0.0
     * @return: the pixel data as an byte array
     */
    unsigned char *getData();

    /**
     * @author: Julian Hinxlage
     * @since: v0.0.0
     * @return: the number of bits uses per pixel
     */
    int getBitsPerPixel();

    /**
     * @author: Julian Hinxlage
     * @since: v0.0.0
     * @return: the pixel value by coordinates, (0,0) is on the top left
     */
    unsigned int getPixel(int x, int y);

private:
    int width;
    int height;
    int offset;
    int bpp;
    std::vector<unsigned char> data;
};


#endif //TEXTRENDERER_BITMAP_HPP
