/*
Copyright (C) 2022  Michel Macke, Finn Wundram, Hassene Jeddi, Mohamed Salah Messai

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef DCS_RASPYADAPTER
#define DCS_RASPYADAPTER

#include "DisplayAdapter.hpp"

#include "RaspberryClient.hpp"

class RaspyAdapter: public DisplayAdapter{
    private:
        std::shared_ptr<RaspberryClient> client;
    
        int displayX;
        int displayY;
    public:
        RaspyAdapter(std::shared_ptr<RaspberryClient> _client, int _displayX, int _displayY);
    
        void update(std::shared_ptr<Framebuffer> source, bool scrolling = false) override;
        int get_width() override;
        int get_height() override;
};
#endif
