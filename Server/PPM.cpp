/*
Copyright (C) 2022  Michel Macke, Finn Wundram, Hassene Jeddi, Mohamed Salah Messai

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include <fstream>
#include <iostream>
#include <cmath>
#include "PPM.hpp"

using namespace std;

PPM::~PPM()
{
    delete_image();
}

PPM::PPM(int height, int width)
{
    set_height(height);
    set_width(width);

    create_image();
}
    
void PPM::save(string name_file)
{
    ofstream output(name_file, ios::binary);

    if(output.is_open())
    {
        output << version << endl;
        output << width << endl;
        output << height << endl;
        output << 255 << endl;
        for (int i = 0; i < height; i++)
        {
            for (int j = 0; j < width; j++)
            {
                output.write((char*) &image[i][j], sizeof(RGB));
            }
        }

        output.close();
    }
}

void PPM::read(string name_file)
{
    ifstream input(name_file, ios::binary);

    if(input.is_open())
    {
        int color;
        char ver[3];

        input.read(ver, 2);

        version = ver;
        input >> width;
        input >> height;
        input >> color;
        input.read(ver,1);

        create_image();

        for (int i = 0; i < height; i++)
        {
            for (int j = 0; j < width; j++)
            {
                input.read((char*) &image[i][j], sizeof(RGB));
            }
        }

        input.close();
        
    }
}

void PPM::create_image()
{
    if (image != nullptr)
        delete_image();
    
    image = new RGB*[height];

    for (int i = 0; i < height; i++)
    {
        image[i] = new RGB[width];

        for (int j = 0; j < width; j++)
        {
            image[i][j].r = 255;
            image[i][j].g = 255;
            image[i][j].b = 255;
        }
    }
}

void PPM::delete_image()
{
    if(image != nullptr)
    {
        for (int i = 0; i < height; i++)
        {
            delete image[i];
        }
        delete image;
    }
}
