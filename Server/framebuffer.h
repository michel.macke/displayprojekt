/*
Copyright (C) 2022  Michel Macke, Finn Wundram, Hassene Jeddi, Mohamed Salah Messai

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef DCS_FRAMEBUFFER
#define DCS_FRAMEBUFFER
#include <vector>
#include <iostream>
#include "Font.hpp"
#include "PPM.hpp"
#include <fstream>

class Framebuffer
{

    private:
    //Groesse des Framebuffers
    int sizex;
    int sizey;
    //Die Farbe, in der Text mit der writeText Methode ausgegeben wird
    unsigned char currentColor[3];

    //Die framebuffer für die verschiedenen Farben
    std::vector<std::vector<unsigned char>> bufferR;
    std::vector<std::vector<unsigned char>> bufferG;
    std::vector<std::vector<unsigned char>> bufferB;

    //Das Font, mit dem gerade durch die writeText methode geschrieben werden kann
    Font currentFont;
    
    //Private Funktionen
    //Liefert einen Vektor mit den Grafischen Bytes des chars character. Diese werden aus currentFont geladen
    std::vector<unsigned char> getCharBytes(char character);


    public:
    //Constructor
    Framebuffer(int sizeX, int sizeY);

    //Destructor
    ~Framebuffer();

    //Getter für Pivate klassenvariablen
    int getWidth()
    {
        return sizex;
    };
    int getHeight()
    {
        return sizey;
    };
    
    std::vector<std::vector<unsigned char>> getBufferR()
    {
        return bufferR;
    };
    std::vector<std::vector<unsigned char>> getBufferG()
    {
        return bufferG;
    };
    std::vector<std::vector<unsigned char>> getBufferB()
    {
        return bufferB;
    };
    int getR()
    {
        return currentColor[0];
    };
    int getG()
    {
        return currentColor[1];
    };
    int getB()
    {
        return currentColor[2];
    };
    Font getFont()
    {
        return currentFont;
    }


    //Setter für private Klassenvariablen
    void setColor(unsigned char r, unsigned char g, unsigned char b)
    {
        currentColor[0] = r;
        currentColor[1] = g;
        currentColor[2] = b;
    };
    void setFont(Font font)
    {
        currentFont = font;
    }

    void setPixel(int x, int y, unsigned char r, unsigned char g, unsigned char b);
    uint8_t getRAtPos(int x, int y);
    uint8_t getGAtPos(int x, int y);
    uint8_t getBAtPos(int x, int y);
    //Methode die Text Bytes in den buffer schreibt. Genauere erklaerung in der .cpp Datei
    void writeGlyphs(std::vector<std::vector<unsigned char>> letters, int startX, int startY);

    //Methode für erstellung des ppms Bild.
    void create_ppm();

    //Erlaub es text in den buffer zu schreiben. Als font wird dort currentFont verwendet
    //UEbergeben werden der text als vektor<char> und die startkoordinaten im buffer 
    void writeText(std::vector<char> text, int startX, int startY);

    void writeMatrix(std::vector<std::vector<unsigned char>> inputR, std::vector<std::vector<unsigned char>> inputG, std::vector<std::vector<unsigned char>> inputB, int startX, int startY);
    
    void writeBild(RGB **image, int height, int width, int xStart = 0, int yStart = 0);

    //Setzen die jeweiligen buffer auf 0 werte zurueck
    void clearBuffers();
    void clearR();
    void clearG();
    void clearB();

    //Gibt die jeweiligen buffer aus
    void printBuffers();
    void printR();
    void printG();
    void printB();
};
#endif
