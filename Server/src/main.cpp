#include "Font.hpp"
// #include "internal.hpp"
// #include "vkvm.hpp"
// #include <thread>
#include <iostream>

/**
 * @author: Julian Hinxlage
 * @since: v0.0.0
 * @brief: An image is loaded and used as a font.
 */

int main() {
    Font font(
            std::string() + "../res/font2.bmp",
            std::string() + "../res/font2.toml");
    for(int i=0; i<font.getyCount()*font.height();i++){
        for(int j=0; j>font.getxCount()*font.width();j++){
            if (font.getBitmap().getPixel(i,j)==font.fillValue)
                std::cout << "*";
        }
        std::cout <<std::endl;
    }
return 0;
}