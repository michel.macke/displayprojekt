#include "Bitmap.hpp"
#include <fstream>
#include <iostream>

Bitmap::Bitmap() {
    offset = 0;
    width = 0;
    height = 0;
    bpp = 0;
}

Bitmap::Bitmap(const std::string &file) {
    offset = 0;
    width = 0;
    height = 0;
    bpp = 0;
    load(file);
}

void Bitmap::load(const std::string &file) {
    std::ifstream stream;
    stream.open(file.c_str());
    if(stream.is_open()){
        std::string str((std::istreambuf_iterator<unsigned char>(stream)),std::istreambuf_iterator<unsigned char>());
        data.resize(str.size());
        for(int i = 0; i < str.size();i++){
            data[i] = str[i];
        }
        offset = *reinterpret_cast<int*>(&data[10]);
        width = *reinterpret_cast<int*>(&data[18]);
        height = *reinterpret_cast<int*>(&data[22]);
        bpp = *reinterpret_cast<uint16_t *>(&data[28]);
    }
}

int Bitmap::getWidth() {
    return width;
}

int Bitmap::getHeight() {
    return height;
}

unsigned char *Bitmap::getData() {
    return &data[offset];
}

int Bitmap::getBitsPerPixel() {
    return bpp;
}

unsigned int Bitmap::getPixel(int x, int y) {
    unsigned int pixel = 0;

    if(bpp == 1){
        int index = (getHeight() - 1 - y) * getWidth() + x;
        unsigned char *ptr = getData() + (index / 8);
        bool value = (ptr[0] >> (7 - (index % 8))) & 1u;
        return (int)value;
    }

    unsigned char *ptr = getData() + ((getHeight() - 1 - y) * getWidth() + x) * (getBitsPerPixel() / 8);
    for(int i = 0; i < getBitsPerPixel() / 8;i++){
        auto value = reinterpret_cast<unsigned char*>(&pixel)+i;
        *value = ptr[i];
    }
    return pixel;
}
