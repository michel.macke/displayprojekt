/*
Copyright (C) 2022  Michel Macke, Finn Wundram, Hassene Jeddi, Mohamed Salah Messai

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "FramebufferController.hpp"

#include "Shapemaker.hpp"

#include <stdio.h>
#include <string>
#include <sstream>

#include "protocol.hpp"
#include "base64.h"

#include "checkImageValidity.hpp"
#include "PreprocessingParser.hpp"
#include "MagickSaveImage.h"
#include "PPM.hpp"



#define FUNCTIONPTXT "Function: printText"
#define FUNCTIONGETSIZE "Function: getSize"
#define FUNCTIONSETPIXEL "Function: setPixel"
#define FUNCTIONSHUTDOWN "Function: stopServer"
#define FUNCTIONLOADIMAGE "Function: loadImage"
#define FUNCTIONUPDATE "Function: updateScreen"
#define FUNCTIONCLEAR "Function: clearScreen"
#define FUNCTIONGETPIXEL "Function: getPixel"
#define FUNCTIONDRAWSHAPE "Function: drawShape"

FramebufferController::FramebufferController(std::shared_ptr<Framebuffer> _buffer, std::shared_ptr<MessageInOut> _input_getter, std::shared_ptr<DisplayAdapter> _display){
    buffer = _buffer;
    input_getter = _input_getter;
    display = _display;
}

void FramebufferController::run(){
    //Serverschleife: Nachricht->Bearbeitung->Antwort
    while( !(this->stopped) ){
        std::vector<uint8_t> msg = this->input_getter->receive();
        
        std::vector<uint8_t> response = handle_msg(msg);
        
        this->input_getter->respond(response);
    }
}


std::vector<uint8_t> FramebufferController::handle_msg(std::vector<uint8_t> msg){

    //check message
    std::istringstream f((char*)msg.data());
    std::string line;
    
    std::vector<uint8_t> response;
    
    //Erste Zeile der Nachricht muss immer die Funktion sein
    if (std::getline(f, line)) {
        if(line==FUNCTIONPTXT){
            response = printText(msg);
            
        }
        else if(line==FUNCTIONSETPIXEL){
            response = setPixel(msg);
        }
        else if(line == FUNCTIONDRAWSHAPE){
            response = drawShape(msg);
        }
        else if(line==FUNCTIONGETPIXEL){
            response = sendPixel(msg);
        }
        else if(line==FUNCTIONLOADIMAGE){
            response = loadImage(msg);
        }
        else if(line==FUNCTIONGETSIZE){
            int displayX = this->display->get_width();
            int displayY = this->display->get_height();
            response = sendDisplaySize(displayX, displayY);

        }
        else if(line==FUNCTIONSHUTDOWN){
            stop();
            std::string responseTxt("Stopping server");
            response = std::vector<uint8_t>(responseTxt.begin(), responseTxt.end());
        }
        else if(line == FUNCTIONUPDATE){
            response = updateScreen(msg);
        }
        else if(line==FUNCTIONCLEAR){
            buffer->clearBuffers();
            std::string responseTxt("Cleared buffer");
            response = std::vector<uint8_t>(responseTxt.begin(), responseTxt.end());
        }
        
        else{
            std::string responseTxt("Unknown function");
            response = std::vector<uint8_t>(responseTxt.begin(), responseTxt.end());
        }
    }
    return response;
}

/*
void FramebufferController(int x, int y, unsigned char r, unsigned char g, unsigned char b)
{

}
*/

std::vector<uint8_t> FramebufferController::loadImage(std::vector<uint8_t> msg){
    //Header verwerfen
    std::string content (msg.begin(), msg.end());
    content = dcs_protocol::getContent(content);
    
    std::stringstream f;
    f.write((char*) content.data(), content.size());
    
    std::string lineInternal;
    std::string substring;
    
    std::getline(f, lineInternal); // Coord
    std::vector<int> coordinates = dcs_protocol::readCoord(lineInternal);
    
    std::getline(f, lineInternal); //Preprocessingsteps
    int steps = dcs_protocol::getNumericAttribute(lineInternal);
    
    std::vector<std::string> preprocessing;
    for(int i = 0; i < steps; i++){
        std::getline(f, lineInternal);
        preprocessing.push_back(dcs_protocol::getStringAttribute(lineInternal));
    }
    
    std::getline(f, lineInternal); //OriginalSize
    //int originalSize = dcs_protocol::getNumericAttribute(lineInternal);
    
    std::getline(f, lineInternal); //EncodedSize
    //int encodedSize = dcs_protocol::getNumericAttribute(lineInternal);
    
    std::getline(f, lineInternal); //EncodedBytes
    std::string file_encoded = dcs_protocol::getStringAttribute(lineInternal);
    
    std::string file_decoded = base64_decode(file_encoded);
    
    std::vector<uint8_t> file_bytes(file_decoded.begin(), file_decoded.end());
    
    std::string response;
    if(validImage(file_bytes)){
        std::string target_file = "./loadedImage.ppm";
    
        file_bytes = preprocessing::applyPreprocessing(file_bytes, preprocessing);
        file_bytes = MagickSaveImage(target_file).apply(file_bytes);
    
        PPM ppm1;
        ppm1.read(target_file);
        this->buffer->writeBild(ppm1.image, ppm1.getHeight(), ppm1.getWidth(), coordinates[0], coordinates[1]);
    
        response = dcs_protocol::addHeader("Response", "Success", "");
    } else {
        response = dcs_protocol::setStringAttribute("Reason", "Invalid image");
        response = dcs_protocol::addHeader("Response", "Failure", response);
    }
    
    return std::vector<uint8_t>(response.begin(), response.end());
}

std::vector<uint8_t> FramebufferController::setPixel(std::vector<uint8_t> msg){

    std::string content (msg.begin(), msg.end());
    content = dcs_protocol::getContent(content);
    
    std::stringstream f;
    f.write((char*) content.data(), content.size());
    
    std::string lineInternal;
    
    std::getline(f, lineInternal);
    std::vector<int> coordinates = dcs_protocol::readCoord(lineInternal);
    
    std::getline(f, lineInternal);
    std::vector<uint8_t> color = dcs_protocol::readColor(lineInternal);
            
    buffer->setPixel(
        coordinates[0], coordinates[1], 
        color[0], color[1], color[2]
    );
    
    std::string response;
    
    response = dcs_protocol::writeCoord(coordinates) + dcs_protocol::writeColor(color) + "\n";
    
    if((coordinates[0] > this->display->get_width() || coordinates[0] < 0) || (coordinates[1] > this->display->get_height() || coordinates[0] < 0)){
        response += dcs_protocol::setStringAttribute("Reason", "Out of bounds");
        response = dcs_protocol::addHeader("Response", "Failure", response);
    } else {
    
        response = dcs_protocol::addHeader("Response", "Success", response);
    }
    
    return std::vector<uint8_t>(response.begin(), response.end());
}

std::vector<uint8_t> FramebufferController::printText(std::vector<uint8_t> msg)
{
    std::string content (msg.begin(), msg.end());
    content = dcs_protocol::getContent(content);
    
    std::stringstream f;
    f.write((char*) content.data(), content.size());
    
    std::string line;
    
    std::getline(f, line);
    std::vector<int> coordinates = dcs_protocol::readCoord(line);
    
    std::getline(f, line);
    std::vector<uint8_t> color = dcs_protocol::readColor(line);
    
    std::getline(f, line);
    std::string text = dcs_protocol::getStringAttribute(line);
    std::vector<char> textVector = std::vector<char>(text.begin(), text.end());
      
    this->buffer->setColor(color[0], color[1], color[2]);
    this->buffer->writeText(textVector, coordinates[0], coordinates[1]);

    
    std::string response = dcs_protocol::addHeader("Response", "Success", "");

    return std::vector<uint8_t>(response.begin(), response.end());
}

std::vector<uint8_t> FramebufferController::sendDisplaySize(int sizeX, int sizeY)
{
   std::string content = dcs_protocol::setNumericAttribute("Width", sizeX);
   content += dcs_protocol::setNumericAttribute("Height", sizeY);
   
   std::string sendVal = dcs_protocol::addHeader("Response", "Success", content);
   return std::vector<uint8_t>(sendVal.begin(), sendVal.end());
}

std::vector<uint8_t> FramebufferController::sendPixel(std::vector<uint8_t> msg)
{
    std::string content (msg.begin(), msg.end());
    content = dcs_protocol::getContent(content);
    
    std::stringstream f;
    f.write((char*) content.data(), content.size());
    
    std::string line;
    
    std::getline(f, line);
    std::vector<int> coordinates = dcs_protocol::readCoord(line);
    
    std::string response;
    
    if((coordinates[0] > this->display->get_width() || coordinates[0] < 0) || (coordinates[1] > this->display->get_height() || coordinates[0] < 0)){
        response = dcs_protocol::writeColor(
            std::vector<uint8_t>({0, 0, 0})
        );
        response += dcs_protocol::setStringAttribute("Reason", "Out of bounds");
        
        response = dcs_protocol::addHeader("Response", "Failure", response);
    } else {
        response = dcs_protocol::writeColor(
            std::vector<uint8_t>({
                buffer->getRAtPos(coordinates[0], coordinates[1]),
                buffer->getGAtPos(coordinates[0], coordinates[1]),
                buffer->getBAtPos(coordinates[0], coordinates[1])
            })
        );
        response = dcs_protocol::addHeader("Response", "Success", response);
    }
    
    return std::vector<uint8_t>(response.begin(), response.end());
}

void FramebufferController::update_display(bool scrolling){
    buffer->create_ppm();
    this->display->update(this->buffer, scrolling);
}

void FramebufferController::stop(){
    this->stopped = true;
}

std::vector<uint8_t> FramebufferController::drawShape(std::vector<uint8_t> msg){
    std::string content (msg.begin(), msg.end());
    content = dcs_protocol::getContent(content);
    
    std::stringstream f;
    f.write((char*) content.data(), content.size());
    
    std::string response;

    
    
    std::string line;
    
    std::getline(f, line); //Shape
    std::string shapeType = dcs_protocol::getStringAttribute(line);
    
    std::getline(f, line); //Color
    std::vector<uint8_t> color = dcs_protocol::readColor(line);
    
    std::getline(f, line); //Points
    int points = dcs_protocol::getNumericAttribute(line);

    std::vector< std::vector< int > > coordinates;
    for(int i = 0; (i < points) && getline(f, line); i++){
        coordinates.push_back(dcs_protocol::readCoord(line));
    }
    
    response += dcs_protocol::setNumericAttribute("Points", coordinates.size());
    
    //Alle gemeinsamen Attribute abgearbeitet, eigene werden in jeweiligen Funktionen geparst 
    
    if(shapeType == LINE){
        //Linie hat nur zwei Punkte
        bresenham_line(
            coordinates[0][0], coordinates[0][1], 
            coordinates[1][0], coordinates[1][1], 
            color[0], color[1], color[2], 
            buffer.get()
        );
    } else if( shapeType == POLYGON){
        
        std::vector<std::vector<int>>::iterator first = coordinates.begin();
        std::vector<std::vector<int>>::iterator second = first + 1;
        
        while(second < coordinates.end()){
            std::vector<int> pointA = *first;
            std::vector<int> pointB = *second;
            
            bresenham_line(
                pointA[0], pointA[1], 
                pointB[0], pointB[1], 
                color[0], color[1], color[2], 
                buffer.get()
            );
            //advance
            first++;
            second++;
        }
        
        
        //first ist nun vor dem Ende
        //second wird zum Anfang gesetzt
        //Kette wird geschlossen
        
        second = coordinates.begin();
        std::vector<int> pointA = *first;
        std::vector<int> pointB = *second;

        bresenham_line(
            pointA[0], pointA[1], 
            pointB[0], pointB[1], 
            color[0], color[1], color[2], 
            buffer.get()
        );
        
    } else if(shapeType == TRIANGLE){
        bresenham_solid(
            coordinates[0][0], coordinates[0][1], 
            coordinates[1][0], coordinates[1][1],
            coordinates[0][0], coordinates[0][1], 
            coordinates[2][0], coordinates[2][1], 
            color[0], color[1], color[2], 
            buffer.get()
        );
    }else if(shapeType == TETRAGON){
        bresenham_solid(
            coordinates[0][0], coordinates[0][1], 
            coordinates[1][0], coordinates[1][1],
            coordinates[3][0], coordinates[3][1], 
            coordinates[2][0], coordinates[2][1], 
            color[0], color[1], color[2], 
            buffer.get()
        );
    } else if(shapeType == CIRCLE){
        std::getline(f, line);
        int radius = dcs_protocol::getNumericAttribute(line);
        
        bresenham_circle(coordinates[0][0], coordinates[0][1], radius, color[0], color[1], color[2], buffer.get());
    } else {
        response = dcs_protocol::setStringAttribute("Reason", "Unknown shape");
        response = dcs_protocol::addHeader("Response", "Failure", response);
        return std::vector<uint8_t>(response.begin(), response.end());
    }

    response = dcs_protocol::addHeader("Response", "Success", response);
    return std::vector<uint8_t>(response.begin(), response.end());
}

std::vector<uint8_t> FramebufferController::updateScreen(std::vector<uint8_t> msg){
    std::string content (msg.begin(), msg.end());
    content = dcs_protocol::getContent(content);
    
    std::stringstream f;
    f.write((char*) content.data(), content.size());
    
    std::string line;
    std::getline(f, line);
    
    bool scrolling = dcs_protocol::getBooleanAttribute(line);
    
    update_display(scrolling);
    std::string response = dcs_protocol::addHeader("Response", "Success", "");
    
    return std::vector<uint8_t>(response.begin(), response.end());
}
