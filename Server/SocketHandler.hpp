/*
Copyright (C) 2022  Michel Macke, Finn Wundram, Hassene Jeddi, Mohamed Salah Messai

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef DCS_SOCKETHANDLER
#define DCS_SOCKETHANDLER

#include "MessageInOut.hpp"

#include <sys/types.h>
#include <sys/socket.h>

#include <netinet/in.h>

#include "LoggingFacility.hpp"

class SocketHandler: public MessageInOut{
    private:
        int server_input_socket;
        struct sockaddr_in server_address;
        int server_address_length;  
        
        std::vector<uint8_t> message_buffer;
        
        struct sockaddr_in client_address;
        int client_address_length;
        
        Logger log;
        
    public:
        SocketHandler(int port, int buffersize, Logger log_);
        
        //MessageInOut
        std::vector<uint8_t> receive() override;
        int respond(std::vector<uint8_t> response) override;

        int get_port();
};
#endif
