//
// Copyright (c) 2019 Julian Hinxlage. All rights reserved.
//

#ifndef TEXTRENDERER_FONT_HPP
#define TEXTRENDERER_FONT_HPP

#include "Bitmap.hpp"

/**
 * @author: Julian Hinxlage
 * @since: v0.0.0
 * @brief: this class provides pixel access based on characters
 */
class Font {
private:
    Bitmap bitmap;

    //space between characters
    int xOffset;
    int yOffset;

    //size of a character
    int xSize;
    int ySize;

    //count of characters per row
    int xCount;
    //count of rows
    int yCount;

    //pixel offset of first character
    int xStart;
    int yStart;

    unsigned int fillValue;
    char firstChar;
    int pixelSize;
    int gap;

    bool invertedColors;

public:

    Font();
    explicit Font(const std::string &file, const std::string &configFile);
    void load(const std::string &file, const std::string &configFile);

    int width();
    int height();
    bool getPixel(char character, int x, int y);

    unsigned int getfillValue();
    int getxCount();
    int getyCount();    
    int getxStart();    
    int getpixelSize();

	int getyStart();
    Bitmap getBitmap();
};


#endif //TEXTRENDERER_FONT_HPP
