/*
Copyright (C) 2022  Michel Macke, Finn Wundram, Hassene Jeddi, Mohamed Salah Messai

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef DCS_RASPBERRYCLIENT
#define DCS_RASPBERRYCLIENT


#include <stdlib.h>
#include <stdio.h>


#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <vector>
#include <string>
#include <iostream>
#include <fstream>

typedef int SOCKET;

class RaspberryClient
{
    private:
        int server_input_port;
        std::vector<char> server_response_buffer;
        std::string ip;
    public:
        RaspberryClient(int port, int buffersize, std::string raspIP);
        
        int getServerInputPort()
        {
            return server_input_port;
        };
        std::string getIP()
        {
            return ip;
        };

        void setIP(std::string newIP)
        {
            ip = newIP;
        };
        void setServerInputPort(int newPort)
        {
            server_input_port = newPort;
        };

        //Returns raspberry pi response
        std::string sendPPM(std::string filepath, bool scroll);
};

#endif
