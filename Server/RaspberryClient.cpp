/*
Copyright (C) 2022  Michel Macke, Finn Wundram, Hassene Jeddi, Mohamed Salah Messai

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "RaspberryClient.hpp"

RaspberryClient::RaspberryClient(int port, int buffersize, std::string raspIP)
{
    ip = raspIP;
    server_input_port = port;
    server_response_buffer=std::vector<char>(buffersize);
}


std::string RaspberryClient::sendPPM(std::string filepath, bool scroll)
{
    SOCKET server_input_socket;

    std::ifstream myfile(filepath);
    std::ifstream input(filepath, std::ios::binary);

    // copies all data into buffer
    std::vector<unsigned char> buffer(std::istreambuf_iterator<char>(input), {});
    char insertval = 0;
    if(scroll)
    {
        insertval = 1;
    }
    //buffer.insert(buffer.begin(), insertval);
    server_input_socket = socket(AF_INET, SOCK_DGRAM, 0);

    struct sockaddr_in server_address;
    server_address.sin_family = AF_INET;                               //Zieladresse angeben
    server_address.sin_port = htons(server_input_port);
    std::cout << buffer.size() << std::endl;
    server_address.sin_addr.s_addr = inet_addr(ip.c_str());

    int server_address_length = sizeof(server_address);


    if(scroll)
        buffer.insert(buffer.begin(), (uint8_t)0);
    else
        buffer.insert(buffer.begin(), (uint8_t)1);

    sendto(server_input_socket, buffer.data(), buffer.size(), 0, (sockaddr*) &server_address, server_address_length);
    printf("Client: Message sent. Waiting for response\n");            //Nachricht an Server senden


    int server_response_status = recvfrom(server_input_socket, server_response_buffer.data(), server_response_buffer.size(), 0, (sockaddr*) &server_address, (socklen_t*) &server_address_length);
    printf("Client: Received server response\n");                      //Auf Rueckmeldung warten
    std::string retVal(server_response_buffer.begin(), server_response_buffer.end());
    return retVal;
}
