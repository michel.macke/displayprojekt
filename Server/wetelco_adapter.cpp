/*
Copyright (C) 2022  Michel Macke, Finn Wundram, Hassene Jeddi, Mohamed Salah Messai

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "wetelco_adapter.h"

#include "wetelco_sp.h"
#include "framebuffer_utils.h"

#include <string>

std::vector<uint8_t> framebuffer_to_wetelco(size_t pixel_amt_x, size_t pixel_amt_y, Framebuffer* buf, int x_offset, int y_offset){
	uint8_t target_display_id = BROADCAST_TO_FULL_DISPLAY;

	std::string page_number ("00");
	uint8_t fade_in_effect = CLEAR_AND_REPLACE;
	uint8_t fade_in_speed = FASTEST;
	uint8_t duration_of_visibility = LONGEST;
	
	std::vector<std::vector<uint8_t>> bitmap_red = byte_to_bit(get_area(buf->getBufferR(), pixel_amt_x, pixel_amt_y, x_offset, y_offset));
	
	std::vector<std::vector<uint8_t>> bitmap_green = byte_to_bit(get_area(buf->getBufferG(), pixel_amt_x, pixel_amt_y, x_offset, y_offset));
	
	std::vector<std::vector<uint8_t>> bitmap_blue = byte_to_bit(get_area(buf->getBufferB(), pixel_amt_x, pixel_amt_y, x_offset, y_offset));
	
	return embed_into_protocol_frame(send_image(page_number, fade_in_effect, fade_in_speed, duration_of_visibility, bitmap_red, bitmap_green, bitmap_blue), target_display_id);
}
