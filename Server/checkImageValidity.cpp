#include "checkImageValidity.hpp"

#include <Magick++.h>

bool validImage(std::vector<uint8_t> bytes){
    try{
        Magick::Blob image_data(bytes.data(), bytes.size());
        Magick::Image image;
        image.read(image_data);
    
        return image.isValid();
    
    } catch (Magick::Exception &error_){
        return false;
    }
}
