/*
Copyright (C) 2022  Michel Macke, Finn Wundram, Hassene Jeddi, Mohamed Salah Messai

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include <fstream>
#include <iostream>
#include <cmath>
#pragma once

using namespace std;

struct RGB
    {
        unsigned char r;
        unsigned char g;
        unsigned char b;
    };

class PPM{
public:
    ~PPM();
    PPM(int height, int width);
    PPM(){};
    
    void set_width(int width){this->width = width;}
    void set_height(int height){this->height = height;}
    void set_version(string version){this->version = version;}

    int getWidth(){return width;}
    int getHeight(){return height;}

    void save(string name_file);
    void read(string name_file);

    void delete_image();

    RGB **image = nullptr;

    string version = "P6";
private:
    
    int width = 0;
    int height = 0;
    void create_image();
};
