/*
Copyright (C) 2022  Michel Macke, Finn Wundram, Hassene Jeddi, Mohamed Salah Messai

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef DCS_FRAMEBUFFERCONTROLLER
#define DCS_FRAMEBUFFERCONTROLLER

#include "server.hpp"

#include <memory>

#include "framebuffer.h"
#include "DisplayAdapter.hpp"
#include "MessageInOut.hpp"

#include <vector>

#include <stdint.h>

class FramebufferController :public Server{
    private:
        bool stopped = false;
        
        std::shared_ptr<Framebuffer> buffer;
        std::shared_ptr<MessageInOut> input_getter;
        std::shared_ptr<DisplayAdapter> display;
        
        std::vector<uint8_t> printText(std::vector<uint8_t> msg);
        std::vector<uint8_t> sendDisplaySize(int sizeX, int sizeY);
        std::vector<uint8_t> setPixel(std::vector<uint8_t> msg);
        std::vector<uint8_t> drawShape(std::vector<uint8_t> msg);

        std::vector<uint8_t> loadImage(std::vector<uint8_t> msg);

        std::vector<uint8_t> sendPixel(std::vector<uint8_t> msg);
        
        std::vector<uint8_t> updateScreen(std::vector<uint8_t> msg);

        
        std::vector<uint8_t> handle_msg(std::vector<uint8_t> msg);

    public:
        FramebufferController(std::shared_ptr<Framebuffer> _buffer, std::shared_ptr<MessageInOut> _input_getter, std::shared_ptr<DisplayAdapter> _display);
    
        void update_display(bool scrolling);
        void run() override;
        void stop() override;
};
#endif
