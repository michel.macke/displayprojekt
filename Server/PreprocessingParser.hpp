#ifndef DCS_PREPROCESSINGPARSER
#define DCS_PREPROCESSINGPARSER

#include <vector>
#include <stdint.h>
#include <string>
namespace preprocessing{

    std::vector<uint8_t> fit(std::vector<uint8_t> file_bytes, std::string preprocessing);
    std::vector<uint8_t> fill(std::vector<uint8_t> file_bytes, std::string preprocessing);
    std::vector<uint8_t> stretch(std::vector<uint8_t> file_bytes, std::string preprocessing);
    std::vector<uint8_t> area(std::vector<uint8_t> file_bytes, std::string preprocessing);
    
    std::vector<uint8_t> applyPreprocessing(std::vector<uint8_t> file_bytes, std::vector<std::string> preprocessing);

}
#endif
