/*
Copyright (C) 2022  Michel Macke, Finn Wundram, Hassene Jeddi, Mohamed Salah Messai

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "RaspyAdapter.hpp"

RaspyAdapter::RaspyAdapter(std::shared_ptr<RaspberryClient> _client, int _displayX, int _displayY){
    client = _client;
    displayX = _displayX;
    displayY = _displayY;
}

void RaspyAdapter::update(std::shared_ptr<Framebuffer> source, bool scrolling){
    source->create_ppm();
    client->sendPPM("./Bild.ppm", scrolling);
}

int RaspyAdapter::get_width(){
    return displayX;
}

int RaspyAdapter::get_height(){
    return displayY;
}
