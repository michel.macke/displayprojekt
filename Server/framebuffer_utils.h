/*
Copyright (C) 2022  Michel Macke, Finn Wundram, Hassene Jeddi, Mohamed Salah Messai

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef DCS_BYTE_TO_BIT
#define DCS_BYTE_TO_BIT
#include <vector>
#include <stdint.h>
#include <stdlib.h>


std::vector< std::vector< uint8_t > > byte_to_bit(std::vector< std::vector< uint8_t > > input, uint8_t threshold = 128);

std::vector< std::vector< uint8_t > > get_area(std::vector< std::vector< uint8_t > > input, size_t len_x, size_t len_y, int x_offset = 0, int y_offset = 0);
std::vector< std::vector< uint8_t > > matrix_into_matrix(std::vector< std::vector< uint8_t > > input, std::vector< std::vector< uint8_t> > result, int x_offset, int y_offset);
#endif
