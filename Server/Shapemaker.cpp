/*
Copyright (C) 2022  Michel Macke, Finn Wundram, Hassene Jeddi, Mohamed Salah Messai

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "Shapemaker.hpp"

#include <stdlib.h>


void bresenham_line(int x_start, int y_start, int x_end, int y_end, uint8_t r, uint8_t g, uint8_t b, Framebuffer* buffer){

    int dx = abs(x_end - x_start);
    int dy = -abs(y_end - y_start);
    
    int sx = (x_start < x_end) ? 1 : -1;
    int sy = (y_start < y_end) ? 1 : -1;
    
    int error = dx + dy;
    int err2;
    
    int x = x_start;
    int y = y_start;
    
    while(true){
    
        buffer->setPixel(x, y, r, g, b);
        
        if(x == x_end && y == y_end){
            break;    //Verlassen falls am Ende angekommen
        }
        
        err2 = 2 * error;
        
        if(err2 > dy){
            error += dy;
            x += sx;
        }
        
        if(err2 < dx){
            error += dx;
            y += sy;
        }
    }

}

void bresenham_solid(int xA, int yA, int xB, int yB,int xC, int yC, int xD, int yD, uint8_t r, uint8_t g, uint8_t b, Framebuffer* buffer){

    //Setup für Linie AB
    int dx_AB = abs(xA - xB);
    int dy_AB = -abs(yA - yB);
    
    int sx_AB = (xA < xB) ? 1 : -1;
    int sy_AB = (yA < yB) ? 1 : -1;
    
    int error_AB = dx_AB + dy_AB;
    int err2_AB;
    
    int x_AB = xA;
    int y_AB = yA;
    
    //Setup für Linie CD
    int dx_CD = abs(xC - xD);
    int dy_CD = -abs(yC - yD);
    
    int sx_CD = (xC < xD) ? 1 : -1;
    int sy_CD = (yC < yD) ? 1 : -1;
    
    int error_CD = dx_CD + dy_CD;
    int err2_CD;
    
    int x_CD = xC;
    int y_CD = yC;
    
    
    
    //Zeichenablauf
    
    while(!(x_AB == xB && y_AB == yB) || !(x_CD == xD && y_CD == yD)){
        if(!(x_AB == xB && y_AB == yB)){
            
            err2_AB = 2 * error_AB;
            
            if(err2_AB > dy_AB){
                error_AB += dy_AB;
                x_AB += sx_AB;
            }
            
            if(err2_AB < dx_AB){
                error_AB += dx_AB;
                y_AB += sy_AB;
            }
        }
        
        
        if(!(x_CD == xD && y_CD == yD)){
            
            err2_CD = 2 * error_CD;
            
            if(err2_CD > dy_CD){
                error_CD += dy_CD;
                x_CD += sx_CD;
            }
            
            if(err2_CD < dx_CD){
                error_CD += dx_CD;
                y_CD += sy_CD;
            }
        }
        
        bresenham_line(x_AB, y_AB, x_CD, y_CD, r, g, b, buffer);
    }
}

void bresenham_circle(int xPoint, int yPoint, int rad, uint8_t r, uint8_t g, uint8_t b, Framebuffer* buffer){

    int error = 1 - rad;
    int ddf_x = 0;
    int ddf_y = -2 * rad;
    
    int x = 0;
    int y = rad;
    
    buffer->setPixel(xPoint + rad, yPoint, r, g, b);
    buffer->setPixel(xPoint - rad, yPoint, r, g, b);
    buffer->setPixel(xPoint, yPoint + rad, r, g, b);
    buffer->setPixel(xPoint, yPoint - rad, r, g, b);
    
    while(x < y){
        if(error >= 0){
            y--;
            ddf_y += 2;
            error += ddf_y;
        }
        x++;
        ddf_x += 2;
        error += ddf_x +1;
        
        buffer->setPixel(xPoint + x, yPoint + y, r, g, b);
        buffer->setPixel(xPoint - x, yPoint + y, r, g, b);
        buffer->setPixel(xPoint + x, yPoint - y, r, g, b);
        buffer->setPixel(xPoint - x, yPoint - y, r, g, b);
        
        buffer->setPixel(xPoint + y, yPoint + x, r, g, b);
        buffer->setPixel(xPoint - y, yPoint + x, r, g, b);
        buffer->setPixel(xPoint + y, yPoint - x, r, g, b);
        buffer->setPixel(xPoint - y, yPoint - x, r, g, b);
    }
}

void bresenham_solid_circle(int xPoint, int yPoint, int rad, uint8_t r, uint8_t g, uint8_t b, Framebuffer* buffer){

    int error = 1 - rad;
    int ddf_x = 0;
    int ddf_y = -2 * rad;
    
    int x = 0;
    int y = rad;
    

    bresenham_line(xPoint + rad, yPoint, xPoint - rad, yPoint, r,g,b, buffer);
    
    bresenham_line(xPoint, yPoint + rad, xPoint, yPoint - rad, r,g,b, buffer);
    
    while(x < y){
        if(error >= 0){
            y--;
            ddf_y += 2;
            error += ddf_y;
        }
        x++;
        ddf_x += 2;
        error += ddf_x +1;
        
        bresenham_line(xPoint + x, yPoint + y, xPoint - x, yPoint + y, r, g, b, buffer);
        
        bresenham_line(xPoint + x, yPoint - y, xPoint - x, yPoint - y, r, g, b, buffer);
        
        bresenham_line(xPoint + y, yPoint + x, xPoint - y, yPoint + x, r, g, b, buffer);
        
        bresenham_line(xPoint + y, yPoint - x, xPoint - y, yPoint - x, r, g, b, buffer);
    }
}
