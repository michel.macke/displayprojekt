/*
Copyright (C) 2022  Michel Macke, Finn Wundram, Hassene Jeddi, Mohamed Salah Messai

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "framebuffer.h"

#include "framebuffer_utils.h"

Framebuffer::Framebuffer(int sizeX, int sizeY)
{
    //Fuellt die buffer mit dem wert 0 auf, wenn der buffer erstellt wird
    this->bufferR = std::vector<std::vector<unsigned char>>(
        sizeY,
        std::vector<unsigned char>(sizeX, 0));

    this->bufferG = std::vector<std::vector<unsigned char>>(
        sizeY,
        std::vector<unsigned char>(sizeX, 0));

    this->bufferB = std::vector<std::vector<unsigned char>>(
        sizeY,
        std::vector<unsigned char>(sizeX, 0));

    //setzen der groeße des buffers in den klassenvariablen
    this->sizex = sizeX;
    this->sizey = sizeY;

    //setzen der farbe auf initialwerte
    currentColor[0] = 1;
    currentColor[1] = 1;
    currentColor[2] = 1;
}

Framebuffer::~Framebuffer()
{
}

void Framebuffer::writeBild(RGB **ppm, int height, int width, int xStart, int yStart) //add scale
{
    for (int y = 0; y < height; y++)
    {
        for (int x = 0; x < width; x++)
        {
            setPixel(x + xStart, y + yStart, ppm[y][x].r, ppm[y][x].g, ppm[y][x].b);
        }
    }
}

/* Args: 
 *   letters: vector gefuellt mit vektoren die jeweils die bytes aus der bitmap fuer jeweils einen Buchstaben beinhalten
 *   startX: x koordinate an der geschrieben werden soll
 *   startY: y koordinate an der geschrieben werden soll
 */
void Framebuffer::writeGlyphs(std::vector<std::vector<unsigned char>> letters, int startX, int startY) //add scale
{

    //Dieser Loop für jeden buchstaben
    for (unsigned int i = 0; i < letters.size(); i++)
    {
        //
        for (unsigned int y = 0; y < (unsigned)(currentFont.height()); y++)
        {
            for (unsigned int x = 0; x < (unsigned)currentFont.width(); x++)
            {
                unsigned char write = 0;

                if (letters[i][((y * currentFont.width()) + x)] > 0)
                {
                    write = 1;
                }
                if(write == 1)
                {
                    bufferR[y + startY][x + startX] = write * currentColor[0];
                    bufferG[y + startY][x + startX] = write * currentColor[1];
                    bufferB[y + startY][x + startX] = write * currentColor[2];
                }
                
            }
        }
        startX += currentFont.width();
    }
}

void Framebuffer::create_ppm()
{
    auto width = 128u, height = 32u;
    height = bufferR.size();
    width = bufferB[0].size();
    //printf("bufferR size is %d and bufferR[0] size is %d \n", bufferR.size(), bufferR[0].size());
    using namespace std;
    ofstream ofs("Bild.ppm", ios_base::out | ios_base::binary);

    ofs << "P6" << endl
        << width << ' ' << height << endl
        << "255" << endl;

    for (auto j = 0u; j < height; j++)
    {
        for (auto i = 0u; i < width; i++)
        {
            char r=0,g=0,b=0;
			r = bufferR[j][i];
			g = bufferG[j][i];
			b = bufferB[j][i];
            ofs << r << g <<b; // red, green, blue
        }
    }
    ofs.close();
}

void Framebuffer::clearBuffers()
{
    clearR();
    clearG();
    clearB();
}

void Framebuffer::clearR()
{
    this->bufferR = std::vector<std::vector<unsigned char>>(
        sizey,
        std::vector<unsigned char>(sizex, 0));
}
void Framebuffer::clearG()
{
    this->bufferG = std::vector<std::vector<unsigned char>>(
        sizey,
        std::vector<unsigned char>(sizex, 0));
}
void Framebuffer::clearB()
{
    this->bufferB = std::vector<std::vector<unsigned char>>(
        sizey,
        std::vector<unsigned char>(sizex, 0));
}

void Framebuffer::printBuffers()
{
    printR();
    printG();
    printB();
}

void Framebuffer::printR()
{
    for (unsigned int x = 0; x < bufferR.size(); x++)
    {
        for (unsigned int y = 0; y < bufferR[x].size(); y++)
        {
            printf("%u", bufferR[x][y]);
        }
        std::cout << std::endl;
    }
}

void Framebuffer::printG()
{
    for (unsigned int x = 0; x < bufferG.size(); x++)
    {
        for (unsigned int y = 0; y < bufferG[x].size(); y++)
        {
            printf("%u", bufferG[x][y]);
        }
        std::cout << std::endl;
    }
}

void Framebuffer::printB()
{
    for (unsigned int x = 0; x < bufferB.size(); x++)
    {
        for (unsigned int y = 0; y < bufferB[x].size(); y++)
        {
            printf("%u", bufferB[x][y]);
        }
        std::cout << std::endl;
    }
}

//Erlaubt es text in den Buffer zu schreiben. Es wird an der Koordinate startX angefangen zu schreiben
void Framebuffer::writeText(std::vector<char> text, int startX, int startY)
{
    //Falls ein negativer Startwert/groesserer wert als displaygroesse uebergeben wird -> return
    if (startX < 0 || startY < 0 || startX > sizex || startY > sizey)
    {
        return;
    }
    std::vector<std::vector<unsigned char>> textBytes = std::vector<std::vector<unsigned char>>(text.size(), std::vector<unsigned char>(currentFont.height() * currentFont.width(), 0));
    for (int i = 0; i < (int)text.size(); i++)
    {
        textBytes[i] = getCharBytes(text[i]);
    }
    writeGlyphs(textBytes, startX, startY);
}

void Framebuffer::writeMatrix(std::vector< std::vector<unsigned char> > inputR, std::vector< std::vector<unsigned char> > inputG, std::vector< std::vector<unsigned char> > inputB, int startX, int startY){
    this->bufferR = matrix_into_matrix(inputR, this->getBufferR(), startX, startY);
    this->bufferG = matrix_into_matrix(inputG, this->getBufferG(), startX, startY);
    this->bufferB = matrix_into_matrix(inputB, this->getBufferB(), startX, startY);
}

void Framebuffer::setPixel(int x, int y, unsigned char r, unsigned char g, unsigned char b)
{
    if(x >= 0 && x < sizex && 
        y >= 0 && y < sizey){
        
        bufferR[y][x] = r;
        bufferG[y][x] = g;
        bufferB[y][x] = b;
    }
}


uint8_t Framebuffer::getRAtPos(int x, int y)
{
    return bufferR[y][x];
}

uint8_t Framebuffer::getGAtPos(int x, int y)
{
    return bufferG[y][x];
}

uint8_t Framebuffer::getBAtPos(int x, int y)
{
    return bufferB[y][x];
}

//Ließt Grafik Bytes aus dem Font für einen character ein
std::vector<unsigned char> Framebuffer::getCharBytes(char character)
{
    std::vector<unsigned char> charBytes = std::vector<unsigned char>(currentFont.height() * currentFont.width(), 0);
    for (int y = 0; y < currentFont.height(); y++)
    {
        for (int x = 0; x < currentFont.width(); x++)
        {
            if (currentFont.getPixel(character, x, y))
            {
                charBytes[y * currentFont.width() + x] = 255;
            }
            else
            {
                charBytes[y * currentFont.width() + x] = 0;
            }
        }
    }
    return charBytes;
}
