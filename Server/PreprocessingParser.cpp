#include "PreprocessingParser.hpp"

#include "MagickResize.h"
#include "MagickExtent.h"

std::vector<std::string> split(std::string input, std::string delim);

//NONE
//FIT Width Height
//FILL Width Height
//STRETCH Width Height
//AREA Width Height Xoffset Yoffset Color


std::vector<uint8_t> preprocessing::fit(std::vector<uint8_t> file_bytes, std::string preprocessing){
    std::vector<std::string> args = split(preprocessing, " ");
    
    int width = std::stoi(args[1]);
    int height = std::stoi(args[2]);
    
    return MagickResize(width, height).apply(file_bytes);
}


std::vector<uint8_t> preprocessing::fill(std::vector<uint8_t> file_bytes, std::string preprocessing){
    std::vector<std::string> args = split(preprocessing, " ");
    
    int width = std::stoi(args[1]);
    int height = std::stoi(args[2]);
    
    return MagickResize(width, height, FILL_AREA).apply(file_bytes);
}


std::vector<uint8_t> preprocessing::stretch(std::vector<uint8_t> file_bytes, std::string preprocessing){
    std::vector<std::string> args = split(preprocessing, " ");
    
    int width = std::stoi(args[1]);
    int height = std::stoi(args[2]);
    
    return MagickResize(width, height, DISTORT).apply(file_bytes);
}


std::vector<uint8_t> preprocessing::area(std::vector<uint8_t> file_bytes, std::string preprocessing){
    std::vector<std::string> args = split(preprocessing, " ");
    
    int x = std::stoi(args[1]);
    int y = std::stoi(args[2]);
    int width = std::stoi(args[3]);
    int height = std::stoi(args[4]);
    uint8_t r = std::stoi(args[5]);
    uint8_t g = std::stoi(args[6]);
    uint8_t b = std::stoi(args[7]);
    
    return MagickExtent(x, y, width, height, r, g, b).apply(file_bytes);
}

std::vector<uint8_t> preprocessing::applyPreprocessing(std::vector<uint8_t> file_bytes, std::vector<std::string> preprocessing){
    for(std::vector<std::string>::iterator iter = preprocessing.begin();
    iter < preprocessing.end();
    iter++){
        std::string step = iter->substr(0, iter->find(" "));
        if(step == "FIT"){
            file_bytes = fit(file_bytes, *iter);
        } else if(step == "FILL"){
            file_bytes = fill(file_bytes, *iter);
        } else if(step == "STRETCH"){
            file_bytes = stretch(file_bytes, *iter);
        } else if(step == "AREA"){
            file_bytes = area(file_bytes, *iter);
        }
    }
    return file_bytes;
}

std::vector<std::string> split(std::string input, std::string delim){
    std::vector<std::string> result;
    
    std::size_t first = 0, second = 0;
    while(second != std::string::npos){
        second = input.find(delim, first);
        result.push_back(input.substr(first, second));
        first = second + 1;
    }
    return result;
}
