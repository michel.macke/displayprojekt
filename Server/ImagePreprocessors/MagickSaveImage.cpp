#include "MagickSaveImage.h"
#include <Magick++.h>

MagickSaveImage::MagickSaveImage(std::string _buffer_file){
    this->buffer_file = _buffer_file;
}

std::vector<uint8_t> MagickSaveImage::apply(std::vector< uint8_t> input){
    Magick::Blob image_data(input.data(), input.size());
    Magick::Image image(image_data);
    
    image.write(this->buffer_file);
    
    Magick::Blob modified_data;
    image.write(&modified_data);
    
    return std::vector<uint8_t>((uint8_t*)modified_data.data(), (uint8_t*)modified_data.data() + modified_data.length());
}
