#include "MagickConvertToPPM.h"
#include <Magick++.h>

MagickConvertToPPM::MagickConvertToPPM(){}

std::vector<uint8_t> MagickConvertToPPM::apply(std::vector<uint8_t> input){
    Magick::Blob image_data(input.data(), input.size());
    Magick::Image image(image_data);
    
    image.magick("PPM");
    
    Magick::Blob modified_data;
    image.write(&modified_data);
    
    return std::vector<uint8_t>((uint8_t*)modified_data.data(), (uint8_t*)modified_data.data() + modified_data.length());
}
