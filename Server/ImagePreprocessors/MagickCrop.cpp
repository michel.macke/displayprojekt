#include "MagickCrop.h"
#include <Magick++.h>

MagickCrop::MagickCrop(unsigned int _width, unsigned int _height, int _x_offset, int _y_offset, uint8_t _flags){
    this->width = _width;
    this->height = _height;
    this->x_offset = _x_offset;
    this->y_offset = _y_offset;
    this->flags = _flags;
}


std::vector<uint8_t> MagickCrop::apply(std::vector<uint8_t> input){
    Magick::Blob image_data(input.data(), input.size());
    Magick::Image image(image_data);
    
    std::string geometry = std::to_string(this->width)
                                            .append(1, 'x')
                                            .append(std::to_string(this->height));
    
    std::string offset_on_x = std::to_string(this->x_offset);
    if( !(offset_on_x[0] == '-') ){
        offset_on_x.insert(offset_on_x.begin(), '+');
    }
    geometry.append(offset_on_x);
    
    std::string offset_on_y = std::to_string(this->y_offset);
    if( !(offset_on_y[0] == '-') ){
        offset_on_y.insert(offset_on_y.begin(), '+');
    }
    geometry.append(offset_on_y);
    
    image.crop(geometry.c_str());
    
    if(!(this->flags & PRESERVE_VIRTUAL_CANVAS)){
        image.repage();
    }
    
    Magick::Blob modified_data;
    image.write(&modified_data);
    
    return std::vector<uint8_t>((uint8_t*)modified_data.data(), (uint8_t*)modified_data.data() + modified_data.length());
}
