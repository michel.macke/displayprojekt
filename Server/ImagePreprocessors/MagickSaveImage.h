#ifndef DCS_MAGICKSAVEIMAGE
#define DCS_MAGICKSAVEIMAGE
#include "ImagePreprocessor.h"

#include <string>

class MagickSaveImage : public ImagePreprocessor{
    private:
        std::string buffer_file;
    public:
        MagickSaveImage(std::string _buffer_file);
        std::vector<uint8_t> apply(std::vector< uint8_t> input) override;
};
#endif
