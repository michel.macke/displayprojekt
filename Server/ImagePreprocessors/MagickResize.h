#ifndef DCS_MAGICKRESIZE
#define DCS_MAGICKRESIZE

#include "ImagePreprocessor.h"
#include <Magick++.h>

#define DISTORT 1
#define FILL_AREA (1 << 1)

#define IF_LARGER (1 << 2)
#define IF_SMALLER (1 << 3)

#define PERCENTAGE (1 << 4)

#define MAX_PIXELS (1 << 5)

class MagickResize : public ImagePreprocessor{
    private:
        unsigned int width;
        unsigned int height;
        uint8_t flags;
    public:
        MagickResize(unsigned int _width, unsigned int _height, uint8_t _flags = 0);
        std::vector<uint8_t> apply(std::vector< uint8_t> input) override;
};

#endif
