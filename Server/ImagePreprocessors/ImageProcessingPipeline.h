#ifndef DCS_IMAGEPROCESSINGPIPELINE
#define DCS_IMAGEPROCESSINGPIPELINE

#include "ImagePreprocessor.h"

#include<memory>

class ImageProcessingPipeline : public ImagePreprocessor{
    private:
        std::vector< std::shared_ptr<ImagePreprocessor>> steps;
    public:
        ImageProcessingPipeline(std::vector< std::shared_ptr<ImagePreprocessor>> _steps);
        std::vector<uint8_t> apply(std::vector< uint8_t> input) override;
};

#endif
