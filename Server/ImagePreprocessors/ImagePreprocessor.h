#ifndef DCS_IMAGEPREPROCESSING
#define DCS_IMAGEPREPROCESSING
#include <vector>
#include <stdint.h>

class ImagePreprocessor{
    public:
        virtual std::vector<uint8_t> apply(std::vector< uint8_t> input) = 0;
};
#endif
