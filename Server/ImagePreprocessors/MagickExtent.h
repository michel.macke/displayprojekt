#ifndef DCS_MAGICKEXTENT
#define DCS_MAGICKEXTENT

#include "ImagePreprocessor.h"

#define CENTER 0
#define NORTH 1
#define NORTHEAST 2
#define EAST 3
#define SOUTHEAST 4
#define SOUTH 5
#define SOUTHWEST 6
#define WEST 7
#define NORTHWEST 8

class MagickExtent: public ImagePreprocessor{
    private:
        unsigned int width;
        unsigned int height;
        int x_offset;
        int y_offset;
        
        uint8_t red;
        uint8_t green;
        uint8_t blue;
        
        uint8_t flags;
    public:
        MagickExtent(unsigned int _width, unsigned int _height, int _x_offset, int _y_offset, uint8_t _red = 0, uint8_t _green = 0, uint8_t _blue = 0, uint8_t _flags = CENTER);
        std::vector<uint8_t> apply(std::vector<uint8_t> input) override;
};

#endif
