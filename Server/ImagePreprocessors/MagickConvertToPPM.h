#ifndef DCS_MAGICKCONVERTTOPPM
#define DCS_MAGICKCONVERTTOPPM
#include "ImagePreprocessor.h"

class MagickConvertToPPM:public ImagePreprocessor{
    public:
        MagickConvertToPPM();
        std::vector<uint8_t> apply(std::vector<uint8_t> input) override;
};

#endif
