#include "ImageProcessingPipeline.h"

ImageProcessingPipeline::ImageProcessingPipeline(std::vector< std::shared_ptr<ImagePreprocessor>> _steps){
    this->steps = _steps;
}

std::vector<uint8_t> ImageProcessingPipeline::apply(std::vector< uint8_t> input){
    std::vector<uint8_t> result = input;
    for(std::vector< std::shared_ptr<ImagePreprocessor>>::iterator iter = this->steps.begin(); iter < this->steps.end(); iter++){
        std::shared_ptr<ImagePreprocessor> ptr = *iter;
        result = ptr->apply(result);
    }
    return result;
}
