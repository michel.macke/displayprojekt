#include "MagickExtent.h"
#include <Magick++.h>


MagickExtent::MagickExtent(unsigned int _width, unsigned int _height, int _x_offset, int _y_offset, uint8_t _red, uint8_t _green, uint8_t _blue, uint8_t _flags){
    this->width = _width;
    this->height = _height;
    this->x_offset = _x_offset;
    this->y_offset = _y_offset;
    
    this->red = _red;
    this->green = _green;
    this->blue = _blue;
    
    this->flags = _flags;
}


std::vector<uint8_t> MagickExtent::apply(std::vector<uint8_t> input){
    Magick::Blob image_data(input.data(), input.size());
    Magick::Image image(image_data);
    
    std::string geometry = std::to_string(this->width)
                                            .append(1, 'x')
                                            .append(std::to_string(this->height));
    
    std::string offset_on_x = std::to_string(this->x_offset);
    if( !(offset_on_x[0] == '-') ){
        offset_on_x.insert(offset_on_x.begin(), '+');
    }
    geometry.append(offset_on_x);
    
    std::string offset_on_y = std::to_string(this->y_offset);
    if( !(offset_on_y[0] == '-') ){
        offset_on_y.insert(offset_on_y.begin(), '+');
    }
    geometry.append(offset_on_y);
    
    Magick::ColorRGB color(
        ((float)this->red/255), 
        ((float)this->green/255), 
        ((float)this->blue/255));
    
    MagickCore::GravityType gravity;
    switch(this->flags){
        case CENTER: gravity = MagickCore::CenterGravity; break;
        case NORTH: gravity = MagickCore::NorthGravity; break;
        case NORTHEAST: gravity = MagickCore::NorthEastGravity; break;
        case EAST: gravity = MagickCore::EastGravity; break;
        case SOUTHEAST: gravity = MagickCore::SouthEastGravity; break;
        case SOUTH: gravity = MagickCore::SouthGravity; break;
        case SOUTHWEST: gravity = MagickCore::SouthWestGravity; break;
        case WEST: gravity = MagickCore::WestGravity; break;
        case NORTHWEST: gravity = MagickCore::NorthWestGravity; break;
    }
    
    image.extent(geometry.c_str(), color, gravity);
    
    
    Magick::Blob modified_data;
    image.write(&modified_data);
    
    return std::vector<uint8_t>((uint8_t*)modified_data.data(), (uint8_t*)modified_data.data() + modified_data.length());
}
