#ifndef DCS_MAGICKCROP
#define DCS_MAGICKCROP

#include "ImagePreprocessor.h"

#define PRESERVE_VIRTUAL_CANVAS 1

class MagickCrop: public ImagePreprocessor{
    private:
        unsigned int width;
        unsigned int height;
        int x_offset;
        int y_offset;
        uint8_t flags;
    public:
        MagickCrop(unsigned int _width, unsigned int _height, int _x_offset = 0, int _y_offset = 0, uint8_t _flags = 0);
        std::vector<uint8_t> apply(std::vector<uint8_t> input) override;
};

#endif
