#include "MagickResize.h"
#include <string>

MagickResize::MagickResize(unsigned int _width, unsigned int _height, uint8_t _flags){
    this->width = _width;
    this->height = _height;
    this->flags = _flags;
}

std::vector<uint8_t> MagickResize::apply(std::vector< uint8_t> input){
    Magick::Blob image_data(input.data(), input.size());
    Magick::Image image(image_data);
    std::string parameters = std::to_string(this->width)
                                            .append(1, 'x')
                                            .append(std::to_string(this->height));
    if(this->flags & DISTORT){
        parameters.append(1, '!');
    }
    if(this->flags & FILL_AREA){
        parameters.append(1, '^');
    }
    if(this->flags & IF_LARGER){
        parameters.append(1, '>');
    }
    if(this->flags & IF_SMALLER){
        parameters.append(1, '>');
    }
    
    /*
    if(this->flags & PERCENTAGE){
        parameters.append(1, '%');
    }
    */
    
    if(this->flags & MAX_PIXELS){
        parameters.append(1, '@');
    }
    
    image.resize(parameters.c_str());
    
    Magick::Blob modified_data;
    image.write(&modified_data);
    
    return std::vector<uint8_t>((uint8_t*)modified_data.data(), (uint8_t*)modified_data.data() + modified_data.length());
}
