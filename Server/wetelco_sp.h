/*
Copyright (C) 2022  Michel Macke, Finn Wundram, Hassene Jeddi, Mohamed Salah Messai

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef WETELCO_SP_H
#define WETELCO_SP_H

//Header file for this projects implementation of the WeTelCo Simple Protocol

#include <stdint.h>
#include <string>
#include <vector>

#define BROADCAST_TO_FULL_DISPLAY 0x00;
std::vector<uint8_t> embed_into_protocol_frame(std::vector<uint8_t> message, uint8_t target_display_id);



//write commands--------------------------------------------------------

//fade_in_effect
#define CLEAR_AND_REPLACE 0x00
#define OVERLAY 0x01

//fade_in_speed
#define SLOWEST 0x00
#define FASTEST 0xFC

//duration_of_visibility
#define SKIP_IMMEDIATELY 0x00
#define SHORTEST 0x01 //-> 500ms
#define LONGEST 0xF0 //-> 120s

std::vector<uint8_t> send_image(std::string page_number, uint8_t fade_in_effect, uint8_t fade_in_speed, uint8_t duration_of_visibility, std::vector< std::vector< uint8_t>> bitmap_red, std::vector< std::vector< uint8_t>> bitmap_green, std::vector< std::vector< uint8_t>> bitmap_blue);


//------------------------------------------------------------------------
#define DISPLAY_ACK 0x06

#define DISPLAY_NACK 0x15
    #define GENERAL_ERROR 0x01
        #define INVALID_PROTOCOL 0x01
        #define RESERVED 0x02
        #define INVALID_COMMAND 0x03
        #define INVALID_PAGE 0x04
        #define OTHER 0x05
        #define INVALID_PLAYLIST_PAGE 0x06
        #define PLAYLIST_TOO_LARGE 0x07
        #define INPUT_BUFFER_OVERFLOW 0x08
        #define NO_EPROM 0x09
        #define NO_MEMORY_CARD 0x0A

    #define TEXT_PROTOCOL_ERROR 0x02
        #define INVALID_FADE_IN_EFFECT 0x01
        #define INVALID_FADI_IN_SPEED 0x02
        #define INVALID_DURATION_OF_VISIBILITY 0x03

    #define GRAPHIC_PROTOCOL_ERROR 0x03
        #define INVALID_NUMBER_OF_PIXELS 0x01


#endif // WETELCO_SP_H
