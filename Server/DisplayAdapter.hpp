/*
Copyright (C) 2022  Michel Macke, Finn Wundram, Hassene Jeddi, Mohamed Salah Messai

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef DCS_DISPLAYADAPTER
#define DCS_DISPLAYADAPTER

#include "framebuffer.h"
#include <memory>

class DisplayAdapter{
    public:
        virtual void update(std::shared_ptr<Framebuffer> source, bool scrolling) = 0;
        virtual int get_width() = 0;
        virtual int get_height() = 0;
};
#endif
