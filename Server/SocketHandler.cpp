/*
Copyright (C) 2022  Michel Macke, Finn Wundram, Hassene Jeddi, Mohamed Salah Messai

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "SocketHandler.hpp"
#include <stdio.h>
#include <unistd.h>

#include <string>

SocketHandler::SocketHandler(int port, int buffersize, Logger log_){
    log = log_;
    this->server_input_socket = socket(AF_INET, SOCK_DGRAM, 0);
    
    this->server_address.sin_family = AF_INET;
    this->server_address.sin_port = htons(port);
    this->server_address.sin_addr.s_addr = INADDR_ANY;
    
    this->server_address_length = sizeof(server_address);
    
    this->client_address_length = sizeof(client_address);
    
    
    if( bind(this->server_input_socket, (sockaddr*) &(this->server_address), this->server_address_length) < 0){
        log->error("SockerHandler: Could not bind to socket.\n");
    }
    
    this->message_buffer = std::vector<uint8_t>(buffersize);
}

std::vector<uint8_t> SocketHandler::receive(){

    if(this->message_buffer.size() == 0){                          //Wenn Nachrichtenbuffer Länge von 0 hat wird sofort eine leere
        log->error("SockerHandler: Input-message buffer has a length of zero\n");      //Nachricht empfangen, dies ist nicht gewollt
    }

    int input_message_status = recvfrom(this->server_input_socket, this->message_buffer.data(), this->message_buffer.size(), 0, (sockaddr*) &(this->client_address), (socklen_t*) &(this->client_address_length));
    
    sleep(1);

    if (input_message_status < 0){
        log->error("SockerHandler: Failed to receive message\n");                      //Fehler augetreten
    } else if (input_message_status == 0){
        log->error("SockerHandler: Received empty message\n");                         //Leer Nachricht
    } else if(message_buffer.size() < input_message_status){
        log->error("SockerHandler: Buffer too small\n");                               //Nachricht größer als der Buffer
    } else {
        std::vector<uint8_t> msg(this->message_buffer.begin(), this->message_buffer.begin() + input_message_status);
        log->info("SocketHandler: Received:\n" + std::string(msg.begin(), msg.end()) + "\n");
        return msg;
    }
    
    std::vector<uint8_t> empty;
    return empty;
}

int SocketHandler::respond(std::vector<uint8_t> response){
    log->info("SocketHandler: Sending response:\n" + std::string(response.begin(), response.end()) + "\n");
    return sendto(
        this->server_input_socket,
        response.data(), 
        response.size(), 
        0, 
        (sockaddr*) &(this->client_address), 
        this->client_address_length
    );
}

int SocketHandler::get_port(){
    return ntohs(this->server_address.sin_port);
}
