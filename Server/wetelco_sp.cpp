/*
Copyright (C) 2022  Michel Macke, Finn Wundram, Hassene Jeddi, Mohamed Salah Messai

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "wetelco_sp.h"

std::vector<uint8_t> embed_into_protocol_frame(std::vector<uint8_t> command, uint8_t target_display_id){
    std::vector<uint8_t> final_packet;

	final_packet.push_back(0x00);
    final_packet.push_back(0x00);   //Frame start
    final_packet.push_back(0x00);

    final_packet.push_back(target_display_id);

    final_packet.insert(final_packet.end(), command.begin(), command.end());    //Insert Content

    final_packet.push_back(0xFE);   //Frame end

    return final_packet;
}


std::vector<uint8_t> send_image(std::string page_number, uint8_t fade_in_effect, uint8_t fade_in_speed, uint8_t duration_of_visibility, std::vector< std::vector< uint8_t>> bitmap_red, std::vector< std::vector< uint8_t>> bitmap_green, std::vector< std::vector< uint8_t>> bitmap_blue){
    std::vector<uint8_t> command;

    command.push_back(0x01);   //Simple Text
    command.push_back(0x02);   //STX
    command.push_back((uint8_t) 'W');    //Write Data
    command.insert(command.end(), page_number.begin(), page_number.end());
    command.push_back(fade_in_effect);
    command.push_back(fade_in_speed);
    command.push_back(duration_of_visibility);

    std::vector< std::vector< uint8_t>>::iterator display_height_iterator;

    for(display_height_iterator = bitmap_red.begin(); display_height_iterator != bitmap_red.end();display_height_iterator++){
        command.insert(command.end(), display_height_iterator->begin(), display_height_iterator->end());
    }

    for(display_height_iterator = bitmap_green.begin(); display_height_iterator != bitmap_green.end();display_height_iterator++){
        command.insert(command.end(), display_height_iterator->begin(), display_height_iterator->end());
    }

    for(display_height_iterator = bitmap_blue.begin(); display_height_iterator != bitmap_blue.end();display_height_iterator++){
        command.insert(command.end(), display_height_iterator->begin(), display_height_iterator->end());
    }

    return command;
}
