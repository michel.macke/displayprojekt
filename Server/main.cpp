/*
Copyright (C) 2022  Michel Macke, Finn Wundram, Hassene Jeddi, Mohamed Salah Messai

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <cstdlib>


#include "./lib/toml/cpptoml.h"
#include "./toml.hpp"

#include "framebuffer.h"
#include "RaspberryClient.hpp"

#include "FramebufferController.hpp"

#include "SocketHandler.hpp"
#include "RaspyAdapter.hpp"

#include "LoggingFacility.hpp"
#include "SimpleFileLogger.hpp"

//Function definitions
int main(int argc, char* argv[]){

   //Parsing of config files
   auto serverConfig = toml::parse_file("../res/server.toml");
   auto framebufferConfig = toml::parse_file("../res/framebuffer.toml");
   auto displayConfig = toml::parse_file("../res/display.toml");
   auto raspberryConfig = toml::parse_file("../res/raspberry.toml");


    //Extract actual values
   std::string fontPath = serverConfig["FontPath"].value_or("");
   std::string fontToml = serverConfig["FontToml"].value_or("");
   int server_input_port = serverConfig["Port"].value_or(25565);
   int buffersize = serverConfig["MaxPacketBufferSize"].value_or(256);


   int framebufferX = framebufferConfig["FramebufferX"].value_or(128);
   int framebufferY = framebufferConfig["FramebufferY"].value_or(32);

   int displayX = displayConfig["SizeX"].value_or(128);
   int displayY = displayConfig["SizeY"].value_or(128);

   int raspPort = raspberryConfig["Port"].value_or(20001);
   int raspBufferSize = raspberryConfig["ResponseBufferSize"].value_or(256);
   std::string raspIP = raspberryConfig["IP"].value_or("127.0.0.1");


   Logger coms_log = std::make_shared<SimpleFileLogger>("./msg_log.txt");

   //Konstruktion der einzelnen Instanzen
   std::shared_ptr<SocketHandler> receiver = std::make_shared<SocketHandler>(server_input_port, buffersize, coms_log);
    
    
   std::shared_ptr<RaspberryClient> client = std::make_shared<RaspberryClient>(raspPort, raspBufferSize, raspIP);
   
   std::shared_ptr<RaspyAdapter> display = std::make_shared<RaspyAdapter>(client, displayX, displayY);


   std::shared_ptr<Framebuffer> buffer = make_shared<Framebuffer>(framebufferX, framebufferY);
   Font font(
      "." + fontPath,
      "." + fontToml);
   buffer->setFont(font);
   
   std::cout << font.width() << std::endl;
   std::cout << font.height() << std::endl;


   std::shared_ptr<FramebufferController> server = make_shared<FramebufferController>(buffer, receiver, display);

   server->run();
   
   return 0;
}
