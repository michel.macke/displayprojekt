//
// Copyright (c) 2019 Julian Hinxlage. All rights reserved.
//

#include "Font.hpp"
#include "./lib/toml/cpptoml.h"

Font::Font() {
    xOffset = 0;
    yOffset = 0;
    xSize = 0;
    ySize = 0;
    xCount = 0;
    yCount = 0;
    xStart = 0;
    yStart = 0;
    fillValue = 0;
    firstChar = ' ';
    pixelSize = 1;
    gap = -1;
    invertedColors = false;
}

Font::Font(const std::string &file, const std::string &configFile) : Font() {
    load(file, configFile);
}

void Font::load(const std::string &file, const std::string &configFile) {
    bitmap.load(file);
    auto config = cpptoml::parse_file(configFile);
    xOffset = config->get_as<int>("xOffset").value_or(0);
    yOffset = config->get_as<int>("yOffset").value_or(0);
    xSize = config->get_as<int>("xSize").value_or(0);
    ySize = config->get_as<int>("ySize").value_or(0);
    xCount = config->get_as<int>("xCount").value_or(0);
    yCount = config->get_as<int>("yOffset").value_or(0);
    xStart = config->get_as<int>("xStart").value_or(0);
    yStart = config->get_as<int>("yStart").value_or(0);
    fillValue = config->get_as<unsigned int>("fillValue").value_or(0);
    firstChar = config->get_as<unsigned char>("firstChar").value_or(0);
    pixelSize = config->get_as<int>("pixelSize").value_or(0);
    gap = config->get_as<int>("gap").value_or(-1);
    invertedColors = static_cast<bool>(config->get_as<int>("invertedColors").value_or(0));
}

int Font::width() {
    return (int)xSize;
}

int Font::height() {
    return (int)ySize;
}

int Font::getxCount() {
    return xCount;
}

int Font::getyCount() {
    return yCount;
}

Bitmap Font::getBitmap(){
    return bitmap;
}

int Font::getxStart() {
    return xStart;
}

int Font::getyStart() {
    return yStart;
}

int Font::getpixelSize(){
	return pixelSize;
}

unsigned int Font::getfillValue(){
    return fillValue;
}

bool Font::getPixel(char character, int x, int y) {
    //index of character(x and y)
    int index = (character - firstChar);

    if(gap != -1){
        if (index >= gap){
            index++;
        }
    }

    int xIndex = index % xCount;
    int yIndex = index / xCount;

    if(index < 0){
        yIndex--;
        xIndex += xCount;
    }

    //character index to pixel index conversion
    int xPos = xIndex * (xSize + xOffset) + xStart;
    int yPos = yIndex * (ySize + yOffset) + yStart;

    bool value = bitmap.getPixel((xPos + x) * pixelSize, (yPos + y) * pixelSize) == fillValue;
    return invertedColors != value;
}

