/*
Copyright (C) 2022  Michel Macke, Finn Wundram, Hassene Jeddi, Mohamed Salah Messai

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "framebuffer_utils.h"

/*
 * Der Framebuffer enthält pro Pixel 8 Bit
 * Das WeTelCo-Display nimmt hingegen pro Pixel nur einen Bit entgegen
 * Da das Programm in Zukunft vielleicht für andere Displays verwendet werden könnte,
 * welche im Farbwert weniger binär sind, wurde dieser Konvertierer geschrieben.
*/

std::vector< std::vector< uint8_t > > byte_to_bit(std::vector< std::vector< uint8_t > > input, uint8_t threshold){
    
    std::vector< std::vector< uint8_t>> result;

    for(std::vector< std::vector< uint8_t>>::iterator input_row_iter = input.begin();
        input_row_iter < input.end();
        input_row_iter++){
        std::vector< uint8_t> result_row;
        
        std::vector< uint8_t>::iterator input_byte_iter = input_row_iter->begin();
        while(input_byte_iter < input_row_iter->end()){
            uint8_t result_byte = 0;
            
            for(int shift = 7; shift > -1 && input_byte_iter < input_row_iter->end(); shift--){
                uint8_t input_byte = *input_byte_iter;
                if(input_byte >= threshold){
                    result_byte |= (1 << shift);
                }
                input_byte_iter++;
            }
            result_row.push_back(result_byte);
        }
        
        result.push_back(result_row);
    }

    return result;
}

std::vector< std::vector< uint8_t > > get_area(std::vector< std::vector< uint8_t > > input, size_t len_x, size_t len_y, int x_offset, int y_offset){
    std::vector< std::vector< uint8_t > > result(len_y, std::vector< uint8_t >(len_x, 0));
    return matrix_into_matrix(input, result, -1*x_offset, -1*y_offset);
}

std::vector< std::vector< uint8_t > > matrix_into_matrix(std::vector< std::vector< uint8_t > > input, std::vector< std::vector< uint8_t> > result, int x_offset, int y_offset){
    unsigned int result_x_offset = 0;
    unsigned int result_y_offset = 0;

    unsigned int input_x_offset = 0;
    unsigned int input_y_offset = 0;

    if(x_offset < 0){
        input_x_offset = x_offset * -1;
    } else {
        result_x_offset = x_offset;
    }

    if(y_offset < 0){
        input_y_offset = y_offset * -1;
    } else {
        result_y_offset = y_offset;
    }
    
    
    std::vector< std::vector< uint8_t > >::iterator result_iter = result.begin() + result_y_offset;

    std::vector< std::vector< uint8_t > >::iterator input_iter = input.begin() + input_y_offset;

    while(result_iter < result.end() && input_iter < input.end()){
        std::vector< uint8_t >::iterator result_row_iter = result_iter->begin() + result_x_offset;
        std::vector< uint8_t >::iterator input_row_iter = input_iter->begin() + input_x_offset;
        
        while(result_row_iter < result_iter->end() && input_row_iter < input_iter->end()){

            *result_row_iter = *input_row_iter;
            result_row_iter++;
            input_row_iter++;
        }

        result_iter++;
        input_iter++;
    }

    return result;
}
