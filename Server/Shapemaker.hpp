/*
Copyright (C) 2022  Michel Macke, Finn Wundram, Hassene Jeddi, Mohamed Salah Messai

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef DCS_SHAPEMAKER
#define DCS_SHAPEMAKER

#include "framebuffer.h"


#include <stdint.h>

void bresenham_line(int x_start, int y_start, int x_end, int y_end, uint8_t r, uint8_t g, uint8_t b, Framebuffer* buffer);

void bresenham_solid(int xA, int yA, int xB, int yB,int xC, int yC, int xD, int yD, uint8_t r, uint8_t g, uint8_t b, Framebuffer* buffer);

void bresenham_circle(int x, int y, int rad, uint8_t r, uint8_t g, uint8_t b, Framebuffer* buffer);

void bresenham_solid_circle(int x, int y, int rad, uint8_t r, uint8_t g, uint8_t b, Framebuffer* buffer);

#endif
