#ifndef DCS_VALIDIMAGE
#define DCS_VALIDIMAGE

#include <vector>
#include <stdint.h>

bool validImage(std::vector<uint8_t> bytes);
#endif
