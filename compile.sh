#!/bin/bash
mkdir ./out
echo compiling the C Server
cd ./Server/
cmake . && make && cp -a server.out ../out/
echo compiling the C Client
cd ../CClient/
cmake . && make && cp -a client.out ../out/
echo compiling the Java Client
cd ../JavaClient/com.ProjektGruppe.maven.eclipse
mvn assembly:assembly -DdescriptorId=jar-with-dependencies -s ci_settings.xml && cp ./target/com.ProjektGruppe.maven.eclipse-0.0.1-SNAPSHOT-jar-with-dependencies.jar ../../out/javaclient.jar
pwd
cd ../../out/
cp -r ../Server/res ./res/
